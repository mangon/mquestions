MATCHES = [
    ('<p>O espaço do mundo é um espaço 2D ou 3D onde ...</p>', 'é definido o modelo a partir de vários objetos gráficos.'),
    ('<p>O espaço do objeto é um espaço 2D ou 3D onde ...</p>', 'é construído um objeto gráfico que participa no modelo.'),
    ('é uma técnica para resolver o problema da oclusão.', None)
]

from mquestions import *

xml_import = category('IMPORT',
    [match('EXEMPLO', MATCHES)])

print(xml_import)
with open('test.xml', 'wt', encoding='utf-8') as f:
    f.write(xml_import)