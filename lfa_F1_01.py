from mquestions import *


F1E1 = [
('F1-'+qid(1, 1),
r'''
Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$, não vazias, em que o primeiro símbolo e o segundo símbolos são diferentes. Por exemplo, $$a, aca$$ estão em $$L$$ enquanto que $$bba, cc$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1, 2),
r'''
Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$, não vazias, em que o primeiro símbolo e o último símbolos são diferentes. Por exemplo, $$ab, bca$$ estão em $$L$$ enquanto que $$b, cc$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1, 3),
r'''
Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$, não vazias, em que o primeiro símbolo e o segundo símbolos são iguais. Por exemplo, $$aa, bbc$$ estão em $$L$$ enquanto que $$baa, c$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1, 4),
r'''
Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$, não vazias, em que o primeiro símbolo e o último símbolos são iguais. Por exemplo, $$aba, b$$ estão em $$L$$ enquanto que $$bac, ca$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1, 5),
r'''
Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$, não vazias, em que não ocorrem símbolos diferentes. Por exemplo, $$a, aa, bbb$$ estão em $$L$$ enquanto que $$accc, bab$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,6),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com, pelo menos, dois símbolos e em que o primeiro e o segundo símbolos são diferentes. Por exemplo, $$abaa, ba, bab$$ estão em $$L$$ enquanto que $$aaa, bba$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,7),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com, pelo menos, dois símbolos e em que o primeiro e o segundo símbolos são iguais. Por exemplo, $$aaa, bba$$ estão em $$L$$ enquanto que $$abaa, bab$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,8),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com, pelo menos, dois símbolos e em que os dois últimos símbolos são iguais. Por exemplo, $$aaa, abb$$ estão em $$L$$ enquanto que $$aaab, ba$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,9),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com, pelo menos, dois símbolos e em que os dois últimos símbolos são diferentes. Por exemplo, $$aba, bba$$ estão em $$L$$ enquanto que $$abaa, bb$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,10),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com comprimento múltiplo de três. Por exemplo, $$\lambda, aba$$ estão em $$L$$ enquanto que $$a, ba, abba$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,11),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ com comprimento ímpar. Por exemplo, $$b, aba$$ estão em $$L$$ enquanto que $$\lambda, a, ba, abba$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,12),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b, c\right\}$$ que não têm subpalavra $$ac$$. Por exemplo, $$\lambda, b, bca$$ estão em $$L$$ enquanto que $$bac, aca$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,13),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ em que cada ocorrência de $$b$$ tem um $$a$$ imediatamente antes. Por exemplo, $$\lambda, ab, aa$$ estão em $$L$$ enquanto que $$b, bab, abb$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
'''),
('F1-'+qid(1,14),
r'''Seja $$L$$ a linguagem das palavras sobre $$\left\{a, b\right\}$$ em que cada ocorrência de $$b$$ tem um $$a$$ imediatamente depois. Por exemplo, $$\lambda, ba, aa$$ estão em $$L$$ enquanto que $$b, bab, abb$$ não estão.
<ol>
    <li>Escreva uma expressão regular que represente $$L$$.</li>
    <li>Defina um autómato finito determinista que reconheça $$L$$.</li>
</ol>
''')
]

#
#   ------------------
#

F1E2 = [
('F1-' + qid(2, 1),
r'''
Considere o AFND $$A = ( \{q_0, q_1, q_2, q_3\}, \{a, b\}, \delta, q_0, \{ q_1\} )$$ com a transição $$\delta$$ definida pela tabela
<p>
$$
\begin{array}{c|ccc}
q & a & b & \lambda \\
\hline
q_0 & q_1 & q_2 & q_1 \\
q_1 & & q_1, q_3 \\
q_2 & & & q_3 \\
q_3 & q_1, q_2, q_3
\end{array}
$$
<ol>
    <li>Aplique o algoritmo dado nas aulas para encontrar um AFD equivalente ao AFND definido acima.</li>
    <li>Aplique algum dos algoritmos dados nas aulas para minimizar o AFD que obteve na alínea anterior.</li>
</ol>
'''),
('F1-' + qid(2, 2),
r'''
Considere o AFND $$A = ( \{q_0, q_1, q_2, q_3\}, \{a, b\}, \delta, q_0, \{ q_3\} )$$ com a transição $$\delta$$ definida pela tabela
<p>
$$
\begin{array}{c|ccc}
q & a & b & \lambda \\
\hline
q_0 & q_1, q_2 \\
q_1 & & q_1, q_3 & \\
q_2 & q_2 & q_2 & q_1 \\
q_3 & q_3 & q_3
\end{array}
$$
<ol>
    <li>Aplique o algoritmo dado nas aulas para encontrar um AFD equivalente ao AFND definido acima.</li>
    <li>Aplique algum dos algoritmos dados nas aulas para minimizar o AFD que obteve na alínea anterior.</li>
</ol>
'''),
('F1-'+qid(2,3),
r'''
Considere o AFND $$A = ( \{q_0, q_1, q_2\}, \{a, b\}, \delta, q_0, \{ q_0, q_1, q_2 \} )$$ com a transição $$\delta$$ definida pela tabela
<p>
$$
\begin{array}{c|ccc}
q & a & b & \lambda \\
\hline
q_0 & q_0, q_1\\
q_1 & & q_1, q_2 & \\
q_2 & & & q_0 q_1
\end{array}
$$
<ol>
    <li>Aplique o algoritmo dado nas aulas para encontrar um AFD equivalente ao AFND definido acima.</li>
    <li>Aplique algum dos algoritmos dados nas aulas para minimizar o AFD que obteve na alínea anterior.</li>
</ol>
'''),
]

f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', F1E1))