LG = [
#
#
#
('Exercício 01', r'''Considere a gramática independente do contexto $$G = (\left\lbrace S,B,C \right\rbrace, \left\lbrace a,b,c \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
    S &\to aS ~|~ bS ~|~ B \\
    B &\to bb ~|~ C ~|~ \lambda \\
    C &\to cC ~|~ \lambda   
\end{align*}$$
<ol>
    <li> Escreva uma expressão regular que represente a linguagem gerada por $$G$$.</li>
    <li> Construa uma gramática (essencialmente) não contraível equivalente.</li>
    <li> Elimine as produções unitárias da gramática obtida na alínea anterior. </li>
</ol>'''),
#
#
#
('Exercício 02', r'''Considere a gramática independente do contexto $$G = (\left\lbrace S,A,B,C \right\rbrace, \left\lbrace a,b,c \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
S &\to ABC ~|~ \lambda \\
A &\to aA ~|~ a \\
B &\to bB ~|~ A \\
C &\to cC ~|~ \lambda
\end{align*}$$
<ol>
    <li> Escreva uma expressão regular que represente a linguagem gerada por $$G$$.</li>
    <li> Construa uma gramática (essencialmente) não contraível equivalente.</li>
    <li> Elimine as produções unitárias da gramática obtida na alínea anterior. </li>
</ol>'''),
#
#
#
('Exercício 03', r'''Considere a gramática independente do contexto $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
S &\to BSA ~|~ A \\
A &\to aA ~|~ \lambda \\
B &\to Bba ~|~ \lambda  
\end{align*}$$
<ol>
    <li> Escreva uma expressão regular que represente a linguagem gerada por $$G$$.</li>
    <li> Construa uma gramática (essencialmente) não contraível equivalente.</li>
    <li> Elimine as produções unitárias da gramática obtida na alínea anterior. </li>
</ol>'''),
#
#
#
('Exercício 04', r'''Construa uma gramática equivalente a $$G = (\left\lbrace S,A,B,C \right\rbrace, \left\lbrace a,b,c \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
    S &\to A ~|~ B ~|~ C \\
    A &\to aa ~|~ B \\
    B &\to bb ~|~ C \\
    C &\to cc ~|~ A 
\end{align*}$$
<p>
que não contenha produções unitárias e escreva uma expressão regular que represente a linguagem gerada por esta gramática.'''),
#
#
#
('Exercício 05', r'''Construa uma gramática equivalente a $$G = (\left\lbrace S,A,B,C,D,E,F,G \right\rbrace, \left\lbrace a,b \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
S &\to aA ~|~ BD \\
A &\to aA ~|~ aAB ~|~ aD \\
B &\to aB ~|~ aC ~|~ BF \\
C &\to Bb ~|~ aAC ~|~ E \\
D &\to bD ~|~ bC ~|~ b \\
E &\to aB ~|~ bC \\
F &\to aF ~|~ aG ~|~ a \\
G &\to a ~|~ b
\end{align*}$$
<p>
sem símbolos inúteis e encontre uma expressão regular que represente a linguagem gerada por esta gramática.'''),
#
#
#
('Exercício 06', r'''Construa uma gramática equivalente a $$G = (\left\lbrace S,A,B,C \right\rbrace, \left\lbrace a,b,c \right\rbrace, P, S)$$, com produções
<p>$$\begin{align*}
    S &\to aAbB ~|~ ABC ~|~ a \\
    A &\to aA ~|~ a \\
    B &\to bBcC ~|~ b \\
    C &\to abc
\end{align*}$$
<p>
na forma normal de Chomsky.'''),
#
#
#
('Exercício 07', r'''Construa uma gramática equivalente a $$G = (\left\lbrace E,S,T \right\rbrace, \left\lbrace a,b,- \right\rbrace, P, E)$$, com produções
<p>$$\begin{align*}
E &\to S \\
S &\to T ~|~ S - T \\
T &\to a ~|~ b
\end{align*}$$
<p>
na forma normal de Greibach.'''),
#
#
#
('Exercício 08', r'''Construa uma gramática equivalente a $$G = (\left\lbrace A,B \right\rbrace, \left\lbrace a,b \right\rbrace, P, A)$$, com produções
<p>$$\begin{align*}
A &\to aAb ~|~ B \\
B &\to Bb ~|~ \lambda
\end{align*}$$
<p>
na forma normal de Greibach.'''),
#
#
#
('Exercício 09', r'''Construa uma gramática equivalente a $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b \right\rbrace, P, S)$$, com produções 
<p>$$\begin{align*}
S &\to  A ~|~ B \\
A &\to AAA ~|~ a ~|~ B \\
B &\to BBb ~|~ b
\end{align*}$$
<p>
na forma normal de Greibach.'''),
]


"""
#
#
#
('Exercício XX', r'''_'''),
"""

from mquestions import *
f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', LG))