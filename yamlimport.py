#! env python3
import os
import re
import random

import pystache

import markdown as md

from ruamel.yaml import YAML
yaml = YAML(typ='safe')

import mquestions as mq
import grids
#
#   CodeRunner
#
PRECHECK = {
    'disabled': 0,
    'empty': 1,
    'examples': 2,
    'selected': 3,
    'all': 4,
}

def proc_testcase(x):
    d = {
        'example': False, 
        'display': True, 
        'stdin': "", 
        'input': "",
        'expected': "",
        'hiderestiffail': True}
    d.update(x)

    return mq.testcase(
        d['input'], 
        d['expected'], 
        d['stdin'], 
        d['example'], 
        d['display'],
        d['hiderestiffail'])
    
def proc_coderunner(q):
    d = {
        'supportfiles': None,
        'answerpreload': "",
        'globalextra': "",
        'answer': "",
        'precheck': "examples",
        'template': "",
        'allornothing': 1,
        'tags': [],
        'attachments': 0,
        'attachmentsrequired': 0,
        'maxfilesize': 10240,
        'filenameregex': "",
        'answerboxlines': 18,
        'answerboxcolumns': 100
    }
    #
    #   HACK UGLY AS F**K BECAUSE OF "answer_preload" and "answerpreload"!!!
    #
    if 'answer_preload' in q.keys() and 'answerpreload' not in q.keys():
        q['answerpreload'] = q['answer_preload']

    d.update(q)
    testcases = [proc_testcase(x) for x in d['testcases']]
    questiontext_html = parse_text(d['questiontext'])
    return mq.coderunner(
        d['name'], 
        questiontext_html, 
        d['answer'],
        testcases, 
        crtype=d['crtype'], 
        precheck=PRECHECK[d['precheck']],
        answer_preload=d['answerpreload'],
        global_extra=d['globalextra'],
        support_files=d['supportfiles'],
        template=d['template'],
        allornothing=d['allornothing'],
        tags=d['tags'],
        attachments=d['attachments'],
        attachmentsrequired=d['attachmentsrequired'],
        maxfilesize=d['maxfilesize'],
        filenameregex=d['filenameregex'],
        answerboxlines=d['answerboxlines'],
        answerboxcolumns=d['answerboxcolumns'])
#
#   Match
#   
#   type: match
#   name: ...
#   questiontext: ...
#   cases
def proc_match(q):
    pass
#
#   MultiChoice
#
#   _type: multichoice
#   name: ...
#   questiontext: ...
#   single: true/false
#   choices:
#       -
#           text: ...
#           fraction: ...
#           [feedback: ...] 
#
multichoice_choice_template = r'''
<answer fraction="{{{fraction}}}" format="html">
    {{{text}}}
    <feedback format="html">
        <text>
            <![CDATA[{{{feedback}}}]]>
        </text>
    </feedback>
</answer>'''



DEFAULT_RIGHTCHOICE = {
    'text': "42",
    'fraction': 100,
    'feedback': "The answer to the ultimate question of life, the universe, and everything."
}

DEFAULT_WRONGCHOICE = {
    'text': "Wrong answer.",
    'fraction': 0,
    'feedback': "No. The other one is the right one."
}

DEFAULT_MULTICHOICE = {
    'name': "Multichoice Question",
    'questiontext': "What is the right answer",
    'single': True,
    'tags': [],
    'choices': [DEFAULT_RIGHTCHOICE, DEFAULT_WRONGCHOICE ]
}


def change(base, **kwargs):
    d = base.copy()
    d.update(kwargs)
    return d

def proc_multichoice_choice(choice):
    conf = {'text': '', 'fraction': 0, 'feedback': ''}
    conf.update(choice)
    answer_text = parse_text(str(conf['text']))
    moodle_text = pystache.render(
        multichoice_choice_template, {
            'text': answer_text,
            'fraction': str(conf['fraction']),
            'feedback': conf['feedback'] }  )
    return moodle_text

def proc_multichoice(q):
    default = {'single': True, 'name': '', 'questiontext': '', 'tags': []}
    default.update(q)
    choices = [proc_multichoice_choice(c) for c in default['choices']]
    moodle_xml = mq.multichoice(
        default['name'],
        parse_text(default['questiontext']),
        choices,
        default['tags'],
        default['single'])
    return moodle_xml

#
#   Pair Choices
#

def pair(a, b):
    return {'a': a, 'b': b}

DEFAULT_PAIRCHOICES = {
    'name': 'Pair Choices', 
    'tags': [],
    'abquestiontext': None,
    'baquestiontext': None,
    'numoptions': 3, 
    'alwaysoption': None,
    'pairs': [
        pair('one', 1),
        pair('two', 2),
        pair('three', 3),
        pair('four', 4),
        pair('five', 5)]}

def proc_pairs(pairs):
    a = [p['a'] for p in pairs]
    b = [p['b'] for p in pairs]
    return pairs, a, b

def render_questions(
        name,
        tags,
        questiontext,
        options, alwaysoption,
        numoptions,
        i,
        ai, bi):
    text = pystache.render(questiontext, {
        "A": ai,
        "B": bi})
    wrong_options = options[:i] + options[i + 1:]
    choices = [change(DEFAULT_WRONGCHOICE, text=t)
        for t in random.sample(wrong_options, numoptions - 1)]
    choices.append(change(
        DEFAULT_RIGHTCHOICE,
        text=options[i]))
    if alwaysoption is not None:
        choices.append(change(
        DEFAULT_WRONGCHOICE,
        text=alwaysoption))
    return change(DEFAULT_MULTICHOICE,
        name=name,
        questiontext=text,
        choices=choices,
        tags=tags)

def proc_pairchoices(q):
    """
    Question text uses {{{A}}} and {{{B}}} to placehold cases

    Example:

    -   type: pairchoices
        name: Pair Choices
        tags: one, two, three
        numoptions: 3
        alwaysoption: |
            None of the other options.
        abquestiontext: |
            A to B. Given A = {{{A}}} what is the B? {{{B}}}.
        baquestiontext: |
            B to A. Given B = {{{A}}} what is the A? {{{A}}}.
        pairs:
            -   a: one
                b: 1
            -   a: two
                b: 2
            -   a: three
                b: 3
            -   a: four
                b: 4
            -   a: five
                b: 5
    """
    q = change(DEFAULT_PAIRCHOICES, **q)
    pairs, a, b = proc_pairs(q['pairs'])
    numoptions = min(len(pairs), q['numoptions'])
    alwaysoption = q['alwaysoption']
    questions = list()
    if q['abquestiontext'] is not None:
        for i, case in enumerate(pairs):
            questions.append(render_questions(
                q['name'],
                ["ab"] + q['tags'],
                q['abquestiontext'],
                b, alwaysoption,
                numoptions, i,
                a[i], b[i])
            )
    if q['baquestiontext'] is not None:
        for i, case in enumerate(pairs):
            questions.append(render_questions(
                q['name'],
                ["ba"] + q['tags'],
                q['baquestiontext'],
                a, alwaysoption,
                numoptions, i,
                a[i], b[i])
            )
    return "\n".join(proc_multichoice(qi) for qi in questions)

def parse_text(text):
    includes = re.compile(r'!\[([^\]]*)\]')
    files = list()
    for m in includes.finditer(text):
        include_file = m[0][2:-1]
        if os.path.isfile(include_file):
            img_element, file_element = mq.embed(
                include_file, 
                os.path.basename(include_file))
            text = text.replace(m[0], img_element)            
            files.append(file_element)
    html_text = md.markdown(text, extensions=[
        'fenced_code',
        'codehilite',
        'tables'])
    moodle_text = mq.questiontext(html_text, ''.join(files))
    return moodle_text
#
#   Gapselect
#
#   type: gapselect
#   name: ...
#   questiontext: ...
#   options:
#     - text: ...
#       group: ...
#
def proc_gapselect_option(option):
    default = {'text': "", 'group': 1}
    default.update(option)
    answer_text = default['text']
    moodle_text = mq.selectoption(answer_text, default['group'])
    return moodle_text

def proc_gapselect(q):
    default = {'name': "", 'questiontext': "", 'tags':[]}
    default.update(q)
    options = [proc_gapselect_option(c) for c in default['options']]
    moodle_xml = mq.gapselect(
        default['name'],
        parse_text(default['questiontext']),
        default['tags'],
        options)
    return moodle_xml
#
#   GridSelect | Special case of GapSelect
#
#   type: gridselect
#   columnseparator: '&'
#   visibleprefix: '@'
#   shufflecolumns: False
#   shufflerows: False
#   name: ...
#   questiontext: ...   
#   rows: 
#       - c01 CS c02 CS ...
#   extraoptions:
#     - (col 0 extra options)
#        - extra option 1
#
def proc_gridselect(q):
    d = {
        'columnseparator': '|',
        'visibleprefix': '^',
        'shufflecolumns': False,
        'shufflerows': False,
        'header': True,
        'name': "",
        'questiontext': "Select the right options\n{{{grid}}}",
        'tags': [],
        'extraoption': []
    }
    d.update(q)
    grid = grids.parse_grid(
            d['grid'],
            columnseparator=d['columnseparator'],
            visibleprefix=d['visibleprefix'])
    grid.header = d['header']
    extra_options = d['extraoptions']
    for g, opts in extra_options.items():
        grid.extend_options(g, opts)
    grid_d = grid.to_dict()
    nl = '\n'
    row_t = ' | '.join(f"{{{{C{c}}}}}" for c in range(grid.shape[1]))
    grid_t = f"""{{{{#grid}}}}{nl}{row_t}{nl}{{{{/grid}}}}"""
    d['questiontext'] = pystache.render(
        d['questiontext'], {'grid': grid_t})
    d['questiontext'] = pystache.render(
        d['questiontext'], grid_d)
    d['options'] = grid_d['options']
    return proc_gapselect(d)
#
#   Essay
#
#   type: essay
#   name: ...
#   questiontext: ...
#

def proc_essay(q):
    default = {
        'name': "", 
        'questiontext': "", 
        'tags': [],}
    default.update(q)
    moodle_xml = mq.essay(
        default['name'],
        parse_text(default['questiontext']),
        default['tags'])
    return moodle_xml
#
# =========================================================
#

QUESTIONTYPE_DISPATCH = {
    'coderunner': proc_coderunner,
    'match': proc_match,
    'multichoice': proc_multichoice,
    'gapselect': proc_gapselect,
    'essay': proc_essay,
    'pairchoices': proc_pairchoices,
    'gridselect': proc_gridselect
}

def proc_question(q):
    return QUESTIONTYPE_DISPATCH[q['type']](q)

def export(d):
    #   Expect d = {xmlfile, category, questions}
    questions = [proc_question(q) for q in d['questions']]
    cat = mq.category(d['category'], questions)
    mq.export(d['xmlfile'], cat)

def yaml2moodle(input_file):
    with open(input_file) as f:
        data = yaml.load(f)
    base, ext = os.path.splitext(input_file)
    data['xmlfile'] = f"{base}.xml"
    export(data)
    
def cli():
    import argparse

    parser = argparse.ArgumentParser(description="Convert YAML to Moodle XML.")
    parser.add_argument('input_file')
    args = parser.parse_args()
    yaml2moodle(args.input_file)

if __name__ == "__main__":
    cli()