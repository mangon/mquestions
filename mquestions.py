import os
import base64
import pystache

tag_template = r'''<tag><text>Antevisão.T03</text>
'''

question_template = r'''
<question type="essay">
    <name>
        <text>{{{NAME}}}</text>
    </name>
    <questiontext format="html">
        <text><![CDATA[<span style="color:red">{{{NAME}}}</span><p>{{{TEXT}}}]]></text>
    </questiontext>
    <responseformat>monospaced</responseformat>
    <responserequired>0</responserequired>
    <responsefieldlines>5</responsefieldlines>
    <attachments>0</attachments>
    <attachmentsrequired>0</attachmentsrequired>
</question>
'''

_question_template = r'''<question type="description">
<name>
<text>{{{NAME}}}</text>
</name>
<questiontext format="html">
<text><![CDATA[<span style="color:red">{{{NAME}}}</span><p>{{{TEXT}}}]]></text>
</questiontext>
<generalfeedback format="html">
<text></text>
</generalfeedback>
<defaultgrade>0.0000000</defaultgrade>
<penalty>0.0000000</penalty>
<hidden>0</hidden>
</question>'''

quiz_template = r'''<?xml version="1.0" encoding="UTF-8" ?>
<quiz>
<question type="category">
<category>
<text>{{{CATEGORY}}}</text>
</category>
<info format="moodle_auto_format">
<text></text>
</info>
</question>
{{{QUESTIONS}}}
</quiz>
'''
# =================================================================
#
#   Code Runner
#
# =================================================================

coderunner_template = r'''<question type="coderunner">
    <name><text>{{{NAME}}}</text></name>
    <questiontext format="html">
      {{{QUESTIONTEXT}}}
    </questiontext>
    <generalfeedback format="html">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
    <idnumber></idnumber>
    <coderunnertype>{{{CRTYPE}}}</coderunnertype>
    <tags>
        {{{TAGS}}}
    </tags>
    <prototypetype>0</prototypetype>
    <allornothing>{{ALLORNOTHING}}</allornothing>
    <penaltyregime>{{{PENALTY}}}</penaltyregime>
    <precheck>{{{PRECHECK}}}</precheck>
    <showsource>0</showsource>
    <answerboxlines>{{{ANSWERBOXLINES}}}</answerboxlines>
    <answerboxcolumns>{{{ANSWERBOXCOLUMNS}}}</answerboxcolumns>
    <answerpreload><![CDATA[{{{ANSWERPRELOAD}}}]]></answerpreload>
    <globalextra><![CDATA[{{{GLOBALEXTRA}}}]]></globalextra>
    <useace></useace>
    <resultcolumns></resultcolumns>
    <template><![CDATA[{{{TEMPLATE}}}]]></template>
    <iscombinatortemplate></iscombinatortemplate>
    <allowmultiplestdins></allowmultiplestdins>
    <answer><![CDATA[{{{ANSWER}}}]]></answer>
    <validateonsave>1</validateonsave>
    <testsplitterre></testsplitterre>
    <language></language>
    <acelang></acelang>
    <sandbox></sandbox>
    <grader></grader>
    <cputimelimitsecs></cputimelimitsecs>
    <memlimitmb></memlimitmb>
    <sandboxparams></sandboxparams>
    <templateparams></templateparams>
    <hoisttemplateparams>1</hoisttemplateparams>
    <twigall>0</twigall>
    <uiplugin></uiplugin>
    <attachments>{{{ATTACHMENTS}}}</attachments>
    <attachmentsrequired>{{{ATTACHMENTSREQUIRED}}}</attachmentsrequired>
    <maxfilesize>{{{MAXFILESIZE}}}</maxfilesize>
    <filenamesregex>{{{FILENAMEREGEX}}}</filenamesregex>
    <filenamesexplain></filenamesexplain>
    <displayfeedback>{{{DISPLAYFEEDBACK}}}</displayfeedback>
    <testcases>
    {{{TESTCASES}}}
    {{{SUPPORTFILES}}}
    </testcases>
  </question>'''

coderunner_case_template = r'''<testcase
    testtype="0"
    useasexample="{{{EXAMPLE}}}"
    hiderestiffail="{{{HIDERESTIFFAIL}}}"
    mark="1.0000000" >
      <testcode>
                <text><![CDATA[{{{CASE}}}]]></text>
      </testcode>
      <stdin>
                <text><![CDATA[{{{STDIN}}}]]></text>
      </stdin>
      <expected>
                <text><![CDATA[{{{EXPECTED}}}]]></text>
      </expected>
      <extra>
                <text></text>
      </extra>
      <display>
                <text><![CDATA[{{{SHOW}}}]]></text>
      </display>
    </testcase>'''

def consolelog(text):
    return f'console.log({text})'

def testcase(
        case, expected,
        stdin='',
        example=False,
        display=True,
        hiderestiffail=True):
    example_str = '1' if example else '0'
    hiderest_str = '1' if hiderestiffail else '0'
    display_str = 'SHOW' if display else 'HIDE'
    return pystache.render(coderunner_case_template, {
            'EXAMPLE': example_str,
            'STDIN': str(stdin),
            'CASE': str(case),
            'EXPECTED': str(expected),
            'SHOW': display_str,
            'HIDERESTIFFAIL': hiderest_str,
        })

def coderunner(
        name, questiontext, answer, cases_xml,
        crtype='nodejs',
        answer_preload='',
        display_feedback="1",
        penalty="10, 20, 30, 40",
        precheck="2",
        global_extra="",
        support_files=None,
        template="",
        allornothing=1,
        tags=[],
        attachments=0,
        attachmentsrequired=0,
        maxfilesize=10240,
        filenameregex="",
        answerboxlines=18,
        answerboxcolumns=100):
    if support_files is None:
        support_files = ""
    else:
        if isinstance(support_files, str):
            support_files = [support_files]
        support_files = '\n'.join(embed(x)[1] for x in support_files)
    return pystache.render(coderunner_template, {
        'NAME':             str(name),
        'CRTYPE':           str(crtype),
        'GLOBALEXTRA':      str(global_extra),
        'QUESTIONTEXT':     str(questiontext),
        'ANSWER':           str(answer),
        'ANSWERPRELOAD':    str(answer_preload),
        'PENALTY':          str(penalty),
        'DISPLAYFEEDBACK':  str(display_feedback),
        'PRECHECK':         str(precheck),
        'TESTCASES':        ''.join(cases_xml),
        'SUPPORTFILES':     support_files,
        'TEMPLATE':         template,
        'ALLORNOTHING':     allornothing,
        'TAGS':             '\n'.join(tag(t) for t in tags),
        'ATTACHMENTS':      attachments,
        'ATTACHMENTSREQUIRED': attachmentsrequired,
        'MAXFILESIZE':      maxfilesize,
        'FILENAMEREGEX':    filenameregex,
        'ANSWERBOXLINES':   answerboxlines,
        'ANSWERBOXCOLUMNS': answerboxcolumns,
    })

# =================================================================
#
#   Multichoice
#
# =================================================================

multichoice_template =r'''<question type="multichoice">
<name>
<text>{{{NAME}}}</text>
</name>
<questiontext format="html">
{{{QUESTIONTEXT}}}
</questiontext>
<defaultgrade>1.0000000</defaultgrade>
<penalty>0.3333333</penalty>
<hidden>0</hidden>
<idnumber></idnumber>
<single>{{{SINGLE}}}</single>
<shuffleanswers>true</shuffleanswers>
<answernumbering>abc</answernumbering>
<correctfeedback format="html">
<text><![CDATA[<p>A sua resposta está correta.</p>]]></text>
</correctfeedback>
<partiallycorrectfeedback format="html">
<text><![CDATA[<p>A sua resposta está parcialmente correta.</p>]]></text>
</partiallycorrectfeedback>
<incorrectfeedback format="html">
<text><![CDATA[<p>A sua resposta está incorreta.</p>]]></text>
</incorrectfeedback>
<tags>
      {{{TAGS}}}
</tags>
{{{ANSWERS}}}
</question>'''

multichoice_answer_template = r'''
<answer fraction="{{{FRACTION}}}" format="html">
    <text>
        <![CDATA[{{{TEXT}}}]]>
    </text>
    <feedback format="html">
        <text>
            <![CDATA[{{{FEEDBACK}}}]]>
        </text>
    </feedback>
</answer>'''

def choice(text, fraction=100, feedback=''):
    moodle_text = pystache.render(multichoice_answer_template, {
        'TEXT':     text,
        'FRACTION': str(fraction),
        'FEEDBACK': feedback,
    })
    return moodle_text

def multichoice(name, text, choices_xml, tags, single=True):
    return pystache.render(multichoice_template, {
        'SINGLE'        : 'true' if single else 'false',
        'NAME'          : name,
        'QUESTIONTEXT'  : text,
        'TAGS'          : '\n'.join(tag(t) for t in tags),
        'ANSWERS'       : '\n'.join(choices_xml),
    })

# =================================================================
#
#   Essay
#
# =================================================================

essay_template =r'''
<question type="essay">
    <name>
      <text>{{{NAME}}}</text>
    </name>
    <questiontext format="html">
{{{QUESTIONTEXT}}}
    </questiontext>
    <generalfeedback format="moodle_auto_format">
      <text></text>
    </generalfeedback>
    <defaultgrade>1.0000000</defaultgrade>
    <penalty>0.0000000</penalty>
    <hidden>0</hidden>
    <idnumber></idnumber>
    <responseformat>monospaced</responseformat>
    <responserequired>1</responserequired>
    <responsefieldlines>5</responsefieldlines>
    <attachments>1</attachments>
    <attachmentsrequired>0</attachmentsrequired>
    <graderinfo format="html">
      <text></text>
    </graderinfo>
    <responsetemplate format="html">
      <text></text>
    </responsetemplate>
    <tags>
        {{{TAGS}}}
    </tags>
</question>'''

def essay(name, text, tags):
    return pystache.render(essay_template, {
        'NAME':         name,
        'QUESTIONTEXT': text,
        'TAGS':         '\n'.join(tag(t) for t in tags),
    })

# =================================================================
#
#   GapSelect
#
# =================================================================

gapselect_template = r'''<question type="gapselect">
<name>
    <text>{{{NAME}}}</text>
</name>
<questiontext format="html">
    {{{QUESTIONTEXT}}}
</questiontext>
<generalfeedback format="html">
    <text></text>
</generalfeedback>
<defaultgrade>1.0000000</defaultgrade>
<penalty>0.3333333</penalty>
<hidden>0</hidden>
<idnumber></idnumber>
<shuffleanswers>true</shuffleanswers>
<correctfeedback format="html">
    <text><![CDATA[<p>A sua resposta está correta.</p>]]></text>
</correctfeedback>
<partiallycorrectfeedback format="html">
    <text><![CDATA[<p>A sua resposta está parcialmente correta.</p>]]></text>
</partiallycorrectfeedback>
<tags>
      {{{TAGS}}}
</tags>
<incorrectfeedback format="html">
    <text><![CDATA[<p>A sua resposta está incorreta.</p>]]></text>
</incorrectfeedback>
{{{SELECTOPTIONS}}}
</question>'''

gapselect_selectoption_template = r'''<selectoption>
    <text>{{{TEXT}}}</text>
    <group>{{{OPTIONGROUP}}}</group>
</selectoption>'''

def selectoption(text, group=0):
    groupstr="123456789"
    moodle_text = pystache.render(gapselect_selectoption_template, {
        'TEXT':         text,
        'OPTIONGROUP':  groupstr[group],
        })
    return moodle_text

def gapselect(name, text, tags, choices_xml):
    return pystache.render(gapselect_template, {
        'NAME':             name,
        'QUESTIONTEXT':     text,
        'SELECTOPTIONS':    '\n'.join(choices_xml),
        'TAGS': '\n'.join(tag(t) for t in tags)        })
# =================================================================
#
#   Match
#
# =================================================================

matching_template = r'''<question type="matching">
<name><text>{{{NAME}}}</text></name>
<questiontext format="html">
{{{QUESTIONTEXT}}}
</questiontext>
<generalfeedback format="html"><text></text></generalfeedback>
<defaultgrade>1.0000000</defaultgrade>
<penalty>0.3333333</penalty>
<hidden>0</hidden>
<idnumber></idnumber>
<shuffleanswers>true</shuffleanswers>
<correctfeedback format="html">
<text><![CDATA[<p>A sua resposta está correta.</p>]]></text>
</correctfeedback>
<partiallycorrectfeedback format="html">
<text><![CDATA[<p>A sua resposta está parcialmente correta.</p>]]></text>
</partiallycorrectfeedback>
<incorrectfeedback format="html">
<text><![CDATA[<p>A sua resposta está incorreta.</p>]]></text>
</incorrectfeedback>
{{{QUESTIONS}}}</question>'''

match_question_and_answer = r'''<subquestion format="html">
    <text><![CDATA[{{{QUESTION}}}]]></text>
    <answer>
    <text>{{{ANSWER}}}</text>
    </answer>
</subquestion>'''

match_answer_only = r'''
<subquestion format="html">
    <answer>
    <text>{{{ANSWER}}}</text>
    </answer>
</subquestion>'''

def matchcase(text, answer):
    if text is None:
        return pystache.render(match_answer_only, {'ANSWER': answer})
    else:
        return pystache.render(match_question_and_answer, {
            'QUESTION': text,
            'ANSWER':   answer,
            })

def match(name, text, cases_xml):
    return pystache.render(matching_template, {
        'NAME':         name,
        'QUESTIONTEXT': text,
        'QUESTIONS':    ''.join(cases_xml),
        })

# =================================================================
#
#   Category - labeled group of questions
#
# =================================================================
def category(label, xml_questions):
    return pystache.render(quiz_template, {
        'CATEGORY':     label,
        'QUESTIONS':    ''.join(xml_questions),
        })

# =================================================================
#
#   Export xml to file
#
# =================================================================
def export(filename, xml_content):
    with open(filename, 'wt', encoding='utf-8') as f:
        f.write(xml_content)

# =================================================================
#
#   Legacy
#
# =================================================================
def question(name, text):
    return pystache.render(question_template, {
        'NAME': name,
        'TEXT': text,
        })

def quiz(category, questions):
    q = list()
    for (n, t) in questions:
        q.append(question(n, t))
    return pystache.render(quiz_template, {
        'CATEGORY':     category,
        'QUESTIONS':    ''.join(q),
        })

# =================================================================
#
#   File Encoding
#
# =================================================================
def embed(filename, remotename=None):
    if remotename is None:
        remotename = os.path.basename(filename)
    with open(filename, 'rb') as f:
        b = f.read()
    str64 = base64.encodebytes(b).decode('utf-8')
    img_element=f'<img src="@@PLUGINFILE@@/{remotename}"></img>'
    file_element=f'<file name="{remotename}" path="/" encoding="base64">{str64}</file>'
    return img_element, file_element

def questiontext(text, extra=''):
    return f'<text><![CDATA[{text}]]></text>\n{extra}'

def tex2html(s):
    pass

def tag(text):
    return f"<tag><text>{text}</text></tag>"

def qid(a, b):
    return str((2**b) * (3**a))
