from mquestions import quiz

q = [
('Exercício 16',
r'''
Considere a expressão regular $$(11 \cup 0)^*(00 \cup 1)^*$$ e o respetivo diagrama. Encontre uma palavra não compatível com esta expressão regular. O que acontece se eliminar o arco-$$\lambda$$ central no diagrama simplificado?
'''),
('Exercício 17',
r'''
<ol>
    <li>Construa e simplifique $${\cal G}\left( a^∗b(c \cup da^* b)^* \right)$$.
    </li>
    <li>Qual é a palavra não vazia mais curta das linguagens definidas pelas expressões:
    <ol>
        <li>$$10 \cup (0 \cup 11)0^* 1$$.</li>
        <li>$$(00 \cup 11 \cup (01 \cup 10)(00 \cup 11)^*(01 \cup 10))^*$$.</li>
        <li>$$\left((00∪11)^* \cup (001 \cup 110)^*\right)^*$$.</li>
    </ol>
    </li>
    <li>Esboce um algoritmo para encontrar a menor palavra numa linguagem regular dada:
    <ol>
        <li>Uma expressão regular.</li>
        <li>O diagrama duma expressão regular.</li>
    </ol>
    </li>
</ol>
'''),
('Exercício 18',r'''<ol>
<li>Encontre diagramas para as seguintes expressões regulares:<ol>
<li>$$(00 \cup 10)(101)^*  \cup 01$$.</li>
<li>$$\left( (00 \cup 11)^* \cup (001 \cup 110)^* \right)^*$$.</li>
<li>$$(a \cup bc^* d)^* bc^*$$.</li>
</ol>
</li>
<li>Determine as expressões regulares definidas pelos diagramas definidos nas seguintes tabelas:<ul>
<li>
<table>
<thead>
<tr>
<th><strong>de</strong></th>
<th><strong>para</strong></th>
<th><strong>aresta</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>0</td>
<td>1</td>
<td>$$\lambda$$</td>
</tr>
<tr>
<td>0</td>
<td>2</td>
<td>$$b$$</td>
</tr>
<tr>
<td>1</td>
<td>1</td>
<td>$$a$$</td>
</tr>
<tr>
<td>1</td>
<td>2</td>
<td>$$\lambda$$</td>
</tr>
<tr>
<td>Inicial</td>
<td></td>
<td></td>
</tr>
<tr>
<td>0</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Finais</td>
<td></td>
<td></td>
</tr>
<tr>
<td>2</td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
</li>
<li>
<table>
<thead>
<tr>
<th><strong>de</strong></th>
<th><strong>para</strong></th>
<th><strong>aresta</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>0</td>
<td>0</td>
<td>$$a$$</td>
</tr>
<tr>
<td>0</td>
<td>1</td>
<td>$$\lambda$$</td>
</tr>
<tr>
<td>0</td>
<td>1</td>
<td>$$b$$</td>
</tr>
<tr>
<td>Inicial</td>
<td></td>
<td></td>
</tr>
<tr>
<td>0</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Finais</td>
<td></td>
<td></td>
</tr>
<tr>
<td>1</td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
</li>
<li>
<table>
<thead>
<tr>
<th><strong>de</strong></th>
<th><strong>para</strong></th>
<th><strong>aresta</strong></th>
</tr>
</thead>
<tbody>
<tr>
<td>0</td>
<td>1</td>
<td>$$\lambda$$</td>
</tr>
<tr>
<td>0</td>
<td>1</td>
<td>$$b$$</td>
</tr>
<tr>
<td>1</td>
<td>1</td>
<td>$$a$$</td>
</tr>
<tr>
<td>Inicial</td>
<td></td>
<td></td>
</tr>
<tr>
<td>0</td>
<td></td>
<td></td>
</tr>
<tr>
<td>Finais</td>
<td></td>
<td></td>
</tr>
<tr>
<td>1</td>
<td></td>
<td></td>
</tr>
</tbody>
</table>
</li>
</ul>
</li>
<li>Determine o menor grafo que representa $$\lambda$$.</li>
<li>Encontre exemplos que mostrem a necessidade das condições de remoção dos arcos-$$\lambda$$.</li>
</ol>'''),
('Exercício 19',
r'''
Construa uma única expressão regular que represente os números reais sem sinal, escritos de acordo com as seguintes regras:
<ul>
    <li>Um número real tem sempre uma vírgula.</li>
    <li>Um número real começa por $$0$$ se e só se a sua parte inteira é $$0$$.</li>
    <li>Um número real acaba em $$0$$ se e só se a sua parte decimal é $$0$$.</li>
</ul>
'''),
('Exercício 20',
r'''
Construa expressões regulares que representem as linguagens indicadas.
<ol>
    <li>A linguagem das palavras sobre $$\left\{a, b, c\right\}$$ em que todos os $$a$$'s precedem todos os $$b$$'s que, por sua vez, precedem todos os $$c$$'s, podendo não haver nem $$a$$'s, nem $$b$$'s, nem $$c$$'s.</li>
    <li>A linguagem da alínea anterior, sem a palavra vazia.</li>
    <li>As palavras sobre $$\left\{a, b, c\right\}$$ de comprimento inferior a $$3$$.</li>
    <li>As palavras sobre $$\left\{a, b, c\right\}$$ que começam por $$a$$, acabam em $$cc$$ e têm exatamente dois $$b$$'s.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ que têm $$aa$$ e $$bb$$ como subpalavras.</li>
    <li>As palavras sobre $$\left\{a, b\right\}$$ de que $$bba$$ não é subpalavra.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ que não têm prefixo $$aaa$$$.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ que não têm $$aaa$$ como subpalavra.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ em que $$ab$$ não ocorre.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ em que $$ab$$ ocorre.</li>
    <li>A linguagem das palavras sobre $$\left\{a, b \right\}$$ em que $$ab$$ ocorre só uma vez.</li>
</ol>
'''),
('Exercício 21',
r'''
Descreva, informalmente, as linguagens representadas pelas seguintes expressões regulares:
<ol>
    <li>$$(a \cup b \cup c)(a \cup b \cup c)^*.$$</li>
    <li>$$(a \cup b)\left( (a \cup b)(b \cup a)\right)^*.$$</li>
    <li>$$5 \cup (1 \cup 2 \cup \cdots \cup 9)(0 \cup 1 \cup \cdots \cup 9)^* (0 \cup 5).$$</li>
    <li>$$c^* (a \cup b)(a \cup b \cup c)^*.$$</li>
    <li>$$\left( a(b \cup c)^* a \cup b \cup c\right)^*.$$</li>
</ol>
'''),
('Exercício 22',
r'''
Simplifique as seguintes expressões regulares:
<ol>
    <li>$$ \emptyset^*  \cup a^* \cup b^*(a \cup b)^*.$$</li>
    <li>$$ aa^* b \cup b.$$</li>
    <li>$$ b^*\left( a \cup (b^* a^*)^*\right) ab^*(ab^*)b.$$</li>
    <li>$$ (a^* b)^* \cup (b^* a)^*.$$</li>
</ol>
'''),
('Exercício 23',
r'''
Encontre uma expressão regular para as palavras binárias
<ol>
    <li> Que representam as potências de $$4$$.</li>
    <li> Com, pelo menos, uma ocorrência de $$001$$.</li>
    <li> Que não têm $$001$$ como subpalavra.</li>
    <li> Com, quanto muito, uma ocorrência de $$00$$ e, quanto muito, uma ocorrência de $$11$$.</li>
    <li> Em que nenhum prefixo tem mais dois $$0$$'s que $$1$$'s nem mais dois $$1$$'s que $$0$$'s.</li>
</ol>
'''),
('Exercício 24',
r'''
Verifique as igualdades
<ol>
    <li> $$0^* (0 \cup 1)^* = (0 \cup 10^*)^*$$.</li>
    <li> $$(10)^+ (0^* 1^* \cup 0^*) = (10)^* 1 0^+ 1^* $$.</li>
</ol>
'''),
('Exercício 25',
r'''

<ol>
    <li>Descreva em linguagem natural as linguagens representadas por:
        <ol>
            <li> $$ \left( 0^* 1^* \right)^* 0$$.</li>
            <li> $$ \left( 01^* \right)^* 0$$.</li>
            <li> $$ \left( 00 \cup 11 \cup (01 \cup 10)(00 \cup 11)^*(01 \cup 10) \right)^*$$.</li>
            <li> $$  0^* \cup (0^* 1 \cup 0^* 11)(0^+ 1 \cup 0^+ 11)^* 0^*$$.</li>
        </ol>
    </li>
    <li>Simplifique:
        <ol>
            <li>$$(00)^* 0 \cup 00^*$$.</li>
            <li>$$(0 \cup 1)(\lambda \cup 00)^+  \cup (0 \cup 1)$$.</li>
            <li>$$ (0 \cup \lambda)0^* 1$$.</li>
        </ol>
    </li>
    <li> Mostre que $$(0^2 \cup 0^3)^* = (0^20^*)^*$$.
    </li>
</ol>
'''),
('Exercício 26',
r'''
Defina expressões regulares para as linguagens binárias das palavras que:
<ol>
    <li>O quinto símbolo a contar da direita é $$0$$.</li>
    <li>Têm $$000$$ ou $$111$$ como subpalavra.</li>
    <li>Não têm $$000$$ ou $$111$$ como subpalavra.</li>
    <li>Não têm $$010$$ como subpalavra.</li>
    <li>Têm um número ímpar de $$0$$'s.</li>
    <li>Têm um número par de ocorrências de $$011$$.</li>
</ol>
''')
]



print(quiz('Exercícios/02 - Expressões Regulares', q))
