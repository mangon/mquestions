import pystache

DEFAULT_RENDERTEMPLATES= {
    'cell.visible': "{{text}}",
    'cell.notvisible': "[[{{group}}]]",
    'row.colsep': "|",
    'grid.rowsep': '\n',
    'table':"{{head}}{{headbodysep}}{{body}}"
}

DEFAULT_CELLFORMAT = lambda c: c.text if c.visible else f"[[{c.pos+1}]]"

class Cell:
    '''Holds the value of a cell in a grid.
    Attributes are:
    - text, of course
    - visible, when this cell is not hidden
    - group, to relate with other options'''

    def __init__(self, text, visible, group=None):
        self.visible = visible
        self.text = text
        self.group = group
        self.pos = None

    def __repr__(self):
        return f"[{'^' if self.visible else '_'}{self.text}{{{self.group}}}]"

    def __hash__(self):
        return hash((self.text, self.group))

    def __eq__(self, other):
        return (self.text == other.text) and (self.group == other.group)

    # def render(self, template=DEFAULT_RENDERTEMPLATES):
    #     t = template['cell.visible'] if self.visible else template['cell.notvisible']
    #     return pystache.render(t,
    #         {
    #             'text': self.text,
    #             'group': self.group
    #         })

class Row:
    '''Holds some Cells'''

    def __init__(self, cells, row=None):
        self.cells = cells
        self.row = row

    def __repr__(self):
        return f"{self.row}. {' '.join(repr(c) for c in self.cells)}"

    def get_cell(self, col=0):
        return self.cells[col] if col < len(self.cells) else None

    def get_cells(self):
        return self.cells

    def get_options(self):
        return {c.group: c.text for c in self.cells if not c.visible}

    def __len__(self):
        return len(self.cells)

    # def render(self, template=DEFAULT_RENDERTEMPLATES):
    #     sep = template['row.colsep']
    #     return sep.join(c.render(template) for c in self.cells)

def fill_row(n, prefix='C'):
    return Row([Cell(f"{prefix}{i}", group=i,visible=True) for i in range(n)])

def unilist_update(a, b):
    return list(set(a) | set(b))

class Grid:
    '''Holds some Rows.'''

    def __init__(self, rows, header=True):
        self.header = header
        self.rows = rows
        options = dict()
        num_cols = 0

        for row in self.rows:
            row_options = row.get_options()
            for g, t in row_options.items():
                if g not in options.keys():
                    options[g] = set()
                options[g].add(t)
            num_cols = max(num_cols, len(row))
        self.shape = (len(self.rows), num_cols)
        self.options = options

    def __repr__(self):
        return '\n'.join(repr(r) for r in self.rows)

    def extend_options(self, group, options):
        if not isinstance(options, set):
            options_set = set(options)
        else:
            options_set = options
        if group not in self.options.keys():
            self.options[group] = set()
        self.options[group] = unilist_update(
            self.options[group],
            options_set)

    # def render(self, template=DEFAULT_RENDERTEMPLATES):
    #     rowsep = template['grid.rowsep']
    #     if not self.header:
    #         lines = rowsep.join(r.render(template) for r in self.rows)
    #     else:
    #         header_row = self.rows[0].render()
    #         body_rows = [r.render(template) for r in self.rows[1:]]
    #         lines = rowsep.join([
    #             header_row, headbody_sep] + body_rows)
    #     return lines

    def freeze_options(self):
        self.options_list = [{'text': text, 'group': group}
                        for group in self.options.keys() for text in self.options[group]]

        for row in self.rows:
            for cell in row.get_cells():
                group = self.options[cell.group]
                cell.pos = 0
                for i,x in enumerate(self.options_list):
                    if x['text'] == cell.text and x['group'] == cell.group:
                        cell.pos = i
                        break

    def to_dict(self, cellformat=DEFAULT_CELLFORMAT):
        self.freeze_options()
        rows = [fill_row(self.shape[1]), Row([])] + self.rows if not self.header else [self.rows[0], Row([])] + self.rows[1:]
        d = {
            'grid': [
                {f"C{cell.group}": cellformat(cell) for cell in row.get_cells()}
                for row in rows],
            'options': self.options_list
        }

        return d


def parse_grid(text,
               rowsseparator='\n',
               columnseparator='&',
               visibleprefix='@'):
    lines = text.split(rowsseparator)
    rows = list()
    for i, line in enumerate(lines):
        if len(line) == 0:
            continue
        cells = list()
        for j, raw_text in enumerate(line.split(columnseparator)):
            raw_text = raw_text.strip()
            if len(raw_text) > 0:
                visible = raw_text[0] == visibleprefix
                text = raw_text[1:] if visible else raw_text
            else:
                visible = True
                text = ""
            cell = Cell(text, visible=visible, group=j)
            cells.append(cell)
        row = Row(cells, row=i)
        rows.append(row)
    return Grid(rows)


if __name__ == "__main__":
    text = r"""a & @b & c
@e & d & f
& &@g"""
    grid = parse_grid(text)
    grid.extend_options(0, set('A'))
    print(grid.options)
