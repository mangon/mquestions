import base64

def encode(filename):
    with open(filename, 'rb') as f:
        bytes = f.read()
    return base64.encodebytes(bytes)

e = encode('triangulo_01.svg')
print(e.decode('utf-8'))
print(base64.decodebytes(e).decode('utf-8'))