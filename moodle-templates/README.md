# Moodle Templates

- `QUESTIONTEXT` is `<text><![CDATA[{{{HTMLSTRING}}}]]></text>`
- Images are:
  - Base64 encoded and utf-8 decoded: `base64.encodebytes(bytes).decode('utf-8')`
  - Embedded via `<file name="{{{REMOTENAME}}}" path="/" encoding="base64">{{{BASE64STRING}}}</file>`
- Default Values (General)
  - `PENALTY`: 0.33
  - `DEFAULTGRADE`: 1.0
  - `SHUFFLEANSWERS`: `true`
  - `ANSWERNUMBERING`: `abc`
  - `CORRECTFEEDBACK`: `<p>Resposta correta.</p>`
  - `PARTIALLYCORRECTFEEDBACK`: `<p>Resposta incompleta.</p>`
  - `INCORRECTFEEDBACK`: `<p>Resposta incorreta.</p>`
  - `PARTIALLYCORRECTFEEDBACK`
- Default Values `coderunner`:
  - `CODERUNNERTYPE`: `nodejs`; other vaures are: `python3`, `java_class`, _etc._
  - `ALLORNOTHING`: 1; What does this mean? What are other values?
  - `DISPLAYFEEDBACK`: 1; What does this mean? What are other values?
  - `PENALTYREGIME`: `"10, 20, 30, 40"`; What does this mean? What are other values?
  - `PRECHECK`: 2; What does this mean? What are other values?
  - `ATTACHMENTS`: 0; What does this mean? What are other values?
  - `ATTACHMENTSREQUIRED`: 0; What does this mean? What are other values?
  - `MAXFILESIZE`: 10240; What does this mean? What are other values?
  - `FILENAMEREGEX`: `""`; What does this mean? What are other values?
- Check:
  - `GENERALFEEDBACK` occurs on which types??
  - What are the values of attribute `format` in text fields?
    - `html`; `moodle_auto_format`; _ore_?

