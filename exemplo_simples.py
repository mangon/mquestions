EXERCICIOS = [
('Exercício 01',
r'''Para o AFD $$A = \left( \{q_0, q_1, q_2\}, \{a, b\}, \delta, q_0, \{q_2\}\right)$$ com a transição $$\delta$$ dada pela tabela
<p>
$$
\begin{array}{c|cc}
    \delta & a & b \\
    \hline
    q_0 & q_0 & q_1 \\
    q_1 & q_2 & q_1 \\
    q_2 & q_2 & q_0
\end{array}
$$
<ol>
    <li>Desenhe o diagrama de estados.</li>
    <li>Escreva as computações de $$A$$ para as palavras $$abaa$$ e $$bbbabb$$ referindo se são, ou não, aceites.</li>
    <li>Escreva uma expressão regular que represente a linguagem reconhecida por $$A$$.</li>
</ol>
'''),
('Exercício 02',
r'''Seja $$A = \left( \{q_0, q_1, q_2, q_3, q_4, q_5\}, \{0, 1\}, \delta, q_0, \{q_4, q_5\}\right)$$ o autómato dado por
<p>
$$
\begin{array}{c|cc}
\delta & 0 & 1 \\
\hline
q_0 & q_1 & q_3 \\
q_1 & q_0 & q_4 \\
q_2 & q_3 & q_1 \\
q_3 & q_2 & q_5 \\
q_4 & q_5 & q_0 \\
q_5 & q_4 & q_2
\end{array}
$$
<ol>
<li>Qual é o estado inicial, e quais são os estados finais?</li>
<li>As palavras $$0001$$, $$010101$$ e $$001110101101$$ são aceites?</li>
<li>Que palavras de $$(01)^*$$ estão em $${\cal L}(A)$$?</li>
<ol>
'''
)
]
from mquestions import *
f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', EXERCICIOS))