from mquestions import *
EX01 = [
    (
        'F3-' + qid(8, 1),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to A ~|~ aSb \\\\
                A &\\to \lambda ~|~ Ac
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 2),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to ab ~|~ aAb \\\\
                A &\\to \lambda ~|~ c ~|~ AA
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 3),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to a ~|~ aAb \\\\
                A &\\to b ~|~ SS
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 4),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to ab ~|~ aAb \\\\
                A &\\to b ~|~ a ~|~ SS
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 5),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to Ac \\\\
                A &\\to B ~|~ AaB \\\\
                B &\\to b ~|~ aSb 
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 6),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to aAb ~|~ a \\\\
                A &\\to Ab ~|~ \lambda
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 7),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to ab ~|~ aAb \\\\
                A &\\to SS ~|~ b
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
    (
        'F3-' + qid(8, 8),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to A\\\\
                A &\\to B ~|~ AaB \\\\
                B &\\to b ~|~ aSb
            \end{align*}
        \)
        <p>
        Use o autómatos dos itens válidos para justificar se a gramática \(G\) é LR(0).'''
    ),
]
f = open('MT04-01.xml', 'wt', encoding='utf-8')
f.write(quiz('IMPORT', EX01))
