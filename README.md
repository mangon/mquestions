# MQuestions

## YAML format

```yaml
category: must
xmlfile: option, defaults to replace this file extension by `xml`
questions: must
    -   type: coderunner
        name: option
        supportfiles: option, one or list of local files
        answerpreload: option
        globalextra: option
        template: ???
        precheck: one of examples, all, selected, empty, disabled
        questiontext: option
        answer: option
        testcases:
            - input: option
            - expected: option
            - stdin: option
            - example: option boolean
            - display: option boolean
    -   type: match (not yet implemented)
    -   type: multichoice
        name: option
        questiontext: option
        single: boolean option
        choices:
            - text: option
            - fraction: float option
            - feedback: option
    -   type: gapselect
        name: option
        questiontext: option
        options:
            -   text: option
                group: option
    -   type: essay
        name: option
        questiontext: option
    -   type: pairchoices
        name: option
        numoptions: int, must
        questiontext: Use {{{A}}} and {{{B}}} to access pair parts.
        pairs:
            -   a: must
                b: must

```
