from mquestions import *

EX01 = [
    ('F2-' + qid(3, 1),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(ab \cup bb)^*$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 2),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(01 \cup 11)^*$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 3),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(ba \cup aa)^*$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 4),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(ab)^* \cup bb$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 5),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(01)^* \cup 11$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 6),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$(ba)^* \cup aa$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 7),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$ab^* \cup aa$$. <em>Na sua resolução use apenas autómatos bem preparados'''),
    ('F2-' + qid(3, 8),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$01^* \cup 00$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>'''),
    ('F2-' + qid(3, 9),
        '''Obtenha um AFND <b>bem preparado</b> que aceite a linguagem <b>complementar</b> da linguagem associada à ER $$ba^* \cup bb$$. <em>Na sua resolução use apenas autómatos bem preparados.</em>''')
]

EX02 = [
    ('F2-' + qid(4, 1),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace a^n c b^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 2),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace 0^n 2 1^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 3),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace a^n b^{n+1} ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 4),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace 0^n 1^{n+1} ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 5),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace a^{n+1} b^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 6),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace 0^{n+1} 1^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 7),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace a a^n b^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 8),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace 0 0^n 1^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
    ('F2-' + qid(4, 9),
'''Defina um AP para reconhecer a linguagem \(L = \left\lbrace a b^n a^n ~:~ n \geq 0 \\right\\rbrace\). Na demonstração que \(L\) não é regular:<ol>
<li>Que palavra $$p$$ usaria?</li>
<li>Que decomposição $$p = uvw$$ consideraria?</li>
<li>Como, a partir daí, concluiria que $$L$$ não é regular?</li></ol>'''),
]

EX03 = [
    ('F2-' + qid(5, 1),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S \\right\}, \left\{ a \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to SS ~|~ a
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 2),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S \\right\}, \left\{ a, b \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to SS ~|~ aSb ~|~ \lambda
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 3),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S \\right\}, \left\{ a \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to SS ~|~ aSa ~|~ \lambda
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 4),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S \\right\}, \left\{ a \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to aa ~|~ SS
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 5),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S, A \\right\}, \left\{ a \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to \lambda ~|~ AS \\\\
A &\\to \lambda ~|~ a
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 6),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S, A \\right\}, \left\{ a \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to SAS ~|~ \lambda \\\\
A &\\to a ~|~ \lambda
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 7),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S \\right\}, \left\{ a, b \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to ab ~|~ SS
\end{align*}\]</p>'''),
    ('F2-' + qid(5, 8),
'''Mostre que a seguinte GIC <b>é ambígua</b>: \(G = \left( \left\{ S, A \\right\}, \left\{ a, b \\right\}, P, S\\right)\) em que as produções de \(P\) são
<p>\[\\begin{align*}
S &\\to Ab ~|~ SS \\\\
A &\\to a ~|~ aA
\end{align*}\]</p>'''),
]
f = open('MT02.xml', 'wt', encoding='utf-8')
f.write(quiz('IMPORT', EX03))
