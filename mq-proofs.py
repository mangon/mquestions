#!/usr/bin/env python3
import os
import csv
import pystache

NOTATION = {
  '&': '∧',
  '|': '∨',
  '~': '¬',
  '>': '→',
  '!': '∀',
  '?': '∃'
}

RULES_HTML = {
  'H': 'H',

  'eC1': '∧-1',
  'eC2': '∧-2',  
  'iC': '∧+',

  'eD': '∨-',
  'iD1': '∨+1',
  'iD2': '∨+2',
  
  'iN': '¬+',
  
  'eI': '→-',
  'iI': '→+',
  
  'eNN': '¬¬-',
  'iNN': '¬¬+',
  
  'eX': '⊥-',
  'iX': '⊥+',
  
  'iT': 'T+',
  
  'MT': 'MT',
  'RA': 'RA',
  'TE': 'TE',

  'iQE': '∃+',
  'eQE': '∃-',

  'iQU': '∀+',
  'eQU': '∀-',

  'i=': '=+',
  'e=': '=-',
}

LEGACY_LANGUAGE = {
  'delimiter': ':',
  'proof_sep': '---',
  'visible': '!',
  'question': '???',
  'qed': 'qed',
  '¬': '¬',
  '∧': '∧',
  '∨': '∨',
  '→': '→',
  '↔': '↔',
  '⊤': '⊤',
  '⊥': '⊥',
  '⊢': '⊢',
  '⊣': '⊣',
  '⊧': '⊧',
  '≡': '≡',
  '∀': '∀',
  '∃': '∃',
  '⊗': '⊗',
  '⊙': '⊙',
}

LATEX_LANGUAGE = {
  'delimiter': '&',
  'proof_sep': '---',
  'visible': '@',
  'question': 'theorem',
  'qed': 'qed',
  '¬': '\\neg ',
  '∧': '\\land',
  '∨': '\\lor',
  '→': '\\to',
  '↔': '\\liff',
  '⊤': '\\top',
  '⊥': '\\bot',
  '⊢': '\\vdash',
  '⊣': '\\dashv',
  '⊧': '\\models',
  '≡': '\\equiv',
  '∀': '\\forall ',
  '∃': '\\exists ',
  '⊗': '\\lxor',
  '⊙': '\\lnand',
}

DEFAULT_QUIZ_TEMPLATE = \
r"""category: {{category}}
questions:
  {{#questions}}
  - type: gapselect
    name: {{name}}
    questiontext: |
      Complete a demonstração de {{{hypothesis}}} ⊢ {{{conclusion}}}:

      ---

      Linha | | Proposição | | Regra | | Base
      :-----|-|:----------:|-|:-----:|-|-----:
      {{#grid}}
      {{line}} || {{{proposition}}} || {{{rule}}} || {{base}}
      {{/grid}}

      ---
    options:
      {{#options}}
      - text: "{{{text}}}"
        group: {{group}}
      {{/options}}
  {{/questions}}
"""

class Cell:
  '''Holds the value of a cell in a grid.
    Attributes are:
    - text, of course
    - visible, when this cell is not hidden
    - group, to relate with other options'''

  def __init__(self, text, group=None, conf=LATEX_LANGUAGE):
    self.conf = conf
    self.visible = text[0] == self.conf['visible']
    self.text = text[1:] if self.visible else text
    self.group = group

  def __repr__(self):
    return f"[{'!' if self.visible else '_'}{self.text}@{{{self.group}}}]"

  def __hash__(self):
    return hash((self.text, self.group))

  def __eq__(self, other):
    return (self.text == other.text) and (self.group == other.group)

  def to_str(self):
    text = self.text
    for k,p in self.conf.items():
      text = text.replace(p, k)
    return text

class Row:
  '''Holds some Cells'''

  def __init__(self, cells, row=None, conf=LATEX_LANGUAGE):
    self.conf = conf
    self.cells = cells
    self.row = row

  def __repr__(self):
    return f"{self.row}. {' '.join(repr(c) for c in self.cells)}"

  def get_cell(self, col=0):
    return self.cells[col] if col < len(self.cells) else None

  def is_question(self, question_keyword=LATEX_LANGUAGE['question']):
    return self.get_cell(0).text == question_keyword

  def is_qed(self, qed_sep=LATEX_LANGUAGE['qed']):
    return self.get_cell(0).text == qed_sep

  def to_str(self):
    return " ".join(c.to_str() for c in self.cells)

  def to_dict(self):
    return {
      'line': self.row + 1,
      'proposition': self.get_cell(0),
      'rule': self.get_cell(1),
      'base': self.get_cell(2)
    } if len(self.cells) >= 3 else dict()

#
# AUX
#
def update_options(cell, target, options):
  '''Avoid repetition of cell.text on target'''

  if cell not in options: # new index
    options.append(cell)
    target.append(len(options) - 1)
  else:
    pos = options.index(cell)
    target.append( pos ) ## point to added reference
  return target, options

class Proof:
  '''Holds some Rows.

      One row separates the proof from additional options.
      In that row:
        Cell #0 holds the question keyword
        Cell #1 holds the hypothesis
        Cell #2 holds the conclusion
  '''

  def __init__(self, rows, conf=LATEX_LANGUAGE):
    self.rows = rows
    found_question = False
    found_qed = False
    self.conf = conf
    for r, row in enumerate(self.rows):
      if row.is_question(self.conf['question']):
        self.hypothesis = row.get_cell(1)
        self.conclusion = row.get_cell(2)
        found_question = True
      if row.is_qed(self.conf['qed']):
        self.proof_rows = self.rows[1:r]
        self.extra_rows = self.rows[r+1:]
        found_qed = True
      if found_question and found_qed:
        break
      

  def __repr__(self):
    head = f"{self.hypothesis} ⊢ {self.conclusion}"
    proof = '\n'.join(repr(r) for r in self.proof_rows)
    extra = '\n'.join(repr(r) for r in self.extra_rows)
    return head + '\n' + proof + '\nqed\n' + extra

  def get_cell(self, row, col):
    return self.rows[row].get_cell(col) if row < len(self.rows) else None

  def to_str(self):
    return "\n".join(r.to_str() for r in self.rows)

  def to_dict(self):
    '''A questiondict is a dict() with keys:
    - 'hypothesis': text
    - 'conclusion': text
    - 'grid': list of rows, where a row is a dict with keys
              'line', 'proposition', 'rule', 'base'
    - 'options': list of options where option is a dict with keys:
              'text', 'group'
    '''

    options = list()
    ans_prop = list()
    ans_rule = list()
    ans_base = list()

    proof_len = len(self.proof_rows)

    for line_num in range(proof_len):

      proof_row = self.proof_rows[line_num]

      # options from propositions
      cell = proof_row.get_cell(0)
      ans_prop, options = update_options(
        cell, ans_prop, options)

      # options from rules
      cell = proof_row.get_cell(1)
      ans_rule, options = update_options(
        cell, ans_rule, options)

      # options from bases
      cell = proof_row.get_cell(2)
      ans_base, options = update_options(
        cell, ans_base, options)

    extra_num = len(self.extra_rows)
    for row in range(extra_num):
      extra_row = self.extra_rows[row]

      for i, cell in enumerate(extra_row.cells):
        if cell not in options:
          if i > 0:
            cell.group = 2
          options.append(cell)

    for rule in RULES_HTML.keys():
      cell = Cell(rule, 1)
      if cell not in options:
        options.append(cell)

    grid = list()
    for r, row in enumerate(self.proof_rows):
      prop = row.get_cell(0)
      rule = row.get_cell(1)
      base = row.get_cell(2)
      row_d = {
        'line': r + 1,
        'proposition': prop.to_str() if prop.visible else ans_prop[r],
        'rule': rule.text if rule.visible else ans_rule[r],
        'base': base.text if base.visible else ans_base[r],
      }
      grid.append(row_d)
      
    return {
      'hypothesis': self.hypothesis,
      'conclusion': self.conclusion,
      'options': options,
      'grid': grid
    }

def parse_rows(rows, conf=LATEX_LANGUAGE):
  return Proof(
    [Row(
      [Cell(cell, c, conf)
        for c, cell in enumerate(row) if len(cell) > 0], r, conf)
    for r, row in enumerate(rows)], conf)

def load_proofs(filename, c=LATEX_LANGUAGE):
  proofs = list()
  rows = list()
  if os.path.exists(filename):    
    with open(filename) as prooffile:
      reader = csv.reader(prooffile, 
        delimiter=c['delimiter'])
      for row in reader:
        cells = [x.strip() for x in row]
        if len(cells) == 1 and cells[0] == c['proof_sep']:
          proofs.append(parse_rows(rows, c))
          rows = list()
        else:
          rows.append(cells)
    proofs.append(parse_rows(rows, c))
  return proofs

def wrap_proof(p, n, name_tpl="Prova {{num}}"):
  d = p.to_dict()
  d.update({'name': pystache.render(name_tpl, {'num': n + 1})})
  d['hypothesis'] = d['hypothesis'].to_str()
  d['conclusion'] = d['conclusion'].to_str()
  grid = list()
  for line in d['grid']:
    p = line['proposition']
    r = line['rule']
    b = line['base']
    grid.append({
      'line': line['line'],
      'proposition': f"[[{p + 1}]]" if type(p) is int else p,
      'rule': f"[[{r + 1}]]" if type(r) is int else RULES_HTML[r],
      'base': f"[[{b + 1}]]" if type(b) is int else b,
    })
  d['grid'] = grid

  options = list()
  for x in d['options']:
    if x.group == 1:
      x.text = RULES_HTML[x.text]
    elif x.group == 0:
      x.text = x.to_str()
    x.group += 1
    options.append(x)
  d['options'] = options

  return d

def render_proofs(proofs,
  category="zzimport",
  template=DEFAULT_QUIZ_TEMPLATE):

  return pystache.render(template, {
    'category': category,
    'questions': [ 
      wrap_proof(proof, i)
      for i, proof in enumerate(proofs)]
  })

if __name__ == '__main__':
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('proofs')
  parser.add_argument('--legacy', '-l',
    default=False,
    action='store_true')
  
  args = parser.parse_args()
  proofs_file = args.proofs
  legacy = args.legacy
  
  base, ext = os.path.splitext(proofs_file)
  yaml_file = f"{base}.yaml"

  language = LEGACY_LANGUAGE if legacy else LATEX_LANGUAGE
  proofs = load_proofs(proofs_file, language)
  with open(yaml_file, "wt") as target:
    yaml_text = render_proofs(
      proofs,
      category=f"zzimports/{base}")
    target.writelines(yaml_text)