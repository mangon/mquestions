import yamlimport as yi
import mquestions as mq
import markdown as md

sample_text = '''
A **imagem seguinte** foi gerada por que instruções?

![triangulo_01.svg]

Indique a sua opção e resolva a seguinte equação:
$$
e^{i\pi} = -1
$$
substituindo cada ocorrência de \(x\) por \(y\)
'''
export_text = sample_text
export_text = md.markdown(export_text)
export_text = yi.parse_text(export_text)

print(export_text)