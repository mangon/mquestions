AFD = [
('Exercício 01',
r'''Para o AFD $$A = \left( \{q_0, q_1, q_2\}, \{a, b\}, \delta, q_0, \{q_2\}\right)$$ com a transição $$\delta$$ dada pela tabela
<p>
$$
\begin{array}{c|cc}
    \delta & a & b \\
    \hline
    q_0 & q_0 & q_1 \\
    q_1 & q_2 & q_1 \\
    q_2 & q_2 & q_0
\end{array}
$$
<ol>
    <li>Desenhe o diagrama de estados.</li>
    <li>Escreva as computações de $$A$$ para as palavras $$abaa$$ e $$bbbabb$$ referindo se são, ou não, aceites.</li>
    <li>Escreva uma expressão regular que represente a linguagem reconhecida por $$A$$.</li>
</ol>
'''),
('Exercício 02',
r'''Seja $$A = \left( \{q_0, q_1, q_2, q_3, q_4, q_5\}, \{0, 1\}, \delta, q_0, \{q_4, q_5\}\right)$$ o autómato dado por
<p>
$$
\begin{array}{c|cc}
\delta & 0 & 1 \\
\hline
q_0 & q_1 & q_3 \\
q_1 & q_0 & q_4 \\
q_2 & q_3 & q_1 \\
q_3 & q_2 & q_5 \\
q_4 & q_5 & q_0 \\
q_5 & q_4 & q_2
\end{array}
$$
<ol>
<li>Qual é o estado inicial, e quais são os estados finais?</li>
<li>As palavras $$0001$$, $$010101$$ e $$001110101101$$ são aceites?</li>
<li>Que palavras de $$(01)^*$$ estão em $${\cal L}(A)$$?</li>
<ol>
'''
),
('Exercício 03',
r'''Seja $$A = \left(
\{0, 1, 2\}, 
\{x, y \}, 
\{
    (0, x, 1), 
    (0, y, 0), 
    (1, x, 1), 
    (1, y, 2), 
    (2, x, 1), 
    (2, y, 0)
 \}, 
0, 
\{0 \}
\right)$$ um AFD.
<ol>
    <li>Desenhe o diagrama de estados.</li>
    <li>Escreva uma expressão regular que represente a linguagem reconhecida por $$A$$.</li>
    <li>Repita a alínea anterior para o AFD $$A'$$ que apenas difere de $$A$$ no conjunto dos estados de aceitação, que no caso de $$A'$$ é $$\{0, 1 \}$$.</li>
</ol>
'''),
('Exercício 04',
r'''
Construa um autómato finito determinista que reconheça a linguagem das palavras sobre $$\{a, b, c\}$$ em que todos os $$a$$'s precedem todos os $$b$$'s que, por sua vez, precedem todos os $$c$$'s (donde que todos os $$a$$'s precedem todos os $$c$$'s), podendo não haver nem $$a$$'s, nem $$b$$'s, nem $$c$$'s.
'''),
('Exercício 05',
r'''Construa um autómato finito determinista que reconheça a linguagem das palavras sobre $$\left\{ a, b \right\}$$ que não têm $$aa$$ como subpalavra.
'''),
('Exercício 06',
r'''Construa um autómato finito determinista que reconheça a linguagem das palavras sobre $$\left\{ a,b,c \right\}$$ em que cada $$b$$ é seguido imediatamente por $$cc$$.
'''),
('Exercício 07',
r'''Construa um autómato finito determinista que reconheça a linguagem das palavras não vazias sobre $$\left\{ a,b \right\}$$ em que o número de $$a$$'s é divisível por $$3$$.
'''),
('Exercício 08',
r'''Construa um autómato finito determinista que reconheça $${\cal L}\left( (ab)^* (ba)^*  \right)$$.
'''),
('Exercício 09',
r'''Indique a linguagem reconhecida pelo AFD definido na seguinte tabela.
<p>
$$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
I   &   0   &   1   &   4   \\
    &   1   &   5   &   2   \\
    &   2   &   3   &   2   \\
F   &   3   &   3   &   3   \\
    &   4   &   5   &   6   \\
    &   5   &   5   &   3   \\
    &   6   &   6   &   6
\end{array}
$$
'''),
('Exercício 10',
r'''Indique a linguagem reconhecida pelo AFD definido na seguinte tabela.
<p>
$$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
IF  &   0   &   0   &   1   \\
    &   1   &   1   &   2   \\
    &   2   &   2   &   3   \\
    &   3   &   3   &   4   \\
    &   4   &   4   &   0 
\end{array}
$$
'''),
('Exercício 11',
r'''(eliminado)'''),
('Exercício 12',
r'''Descreva as linguagens reconhecidas pelos AFDs seguintes:
<ol>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
IF  &   0   &   1   &   3   \\
    &   1   &   4   &   3   \\
    &   2   &   1   &   0   \\
    &   3   &   2   &   4   \\
    &   4   &   0   &   2 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
I   &   0   &   1   &   2   \\
    &   1   &   0   &   3   \\
F   &   2   &   3   &   0   \\
    &   3   &   2   &   1 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
I   &   0   &   1   &   0   \\
    &   1   &   1   &   2   \\
    &   2   &   3   &   0   \\
F   &   3   &   4   &   4   \\
    &   4   &   4   &   4 
\end{array}
    $$
    </li>
</ol> 
'''),
('Exercício 13',
r'''Encontre um AFD que aceite a linguagem das palavras:
<ol>
<li>Compatíveis com $$(0 \cup 1)^* $$.</li>
<li>Com prefixo $$01$$.</li>
<li>Com subpalavra $$00101$$.</li>
<li>Expansões binárias dos naturais congruentes zero módulo $$5$$.</li>
<li>Com subpalavra $$00$$ e sufixo $$01$$.</li>
<li>Com subpalavra $$00$$ e não têm sufixo $$01$$.</li>
<li>Em que cada bloco de quatro letras consecutivas contém $$01$$.</li>
</ol>
'''),
('Exercício 14',
r'''Encontre um AFD  que aceite a linguagem das
<ol>
<li>Com prefixo $$010$$.</li>
<li>Com sufixo $$101$$.</li>
<li>Com subpalavra $$010$$ ou $$101$$.</li>
<li>Em que as últimas cinco letras têm três $$0$$s.</li>
<li>$$w$$ tais que $$\left| w \right|_1 + 2\left| w \right|_0$$ é múltiplo de cinco (nota: $$\left| w \right|_x$$ é o número de ocorrências da letra $$x$$ na palavra $$w$$.).</li>
<li>Sobre o alfabeto $$\left\{ 1, 2, 3 \right\}$$ onde a soma dos símbolos é múltiplo de cinco.</li>
</ol>
''')
]

AFND = [
('Exercício 15',
r'''Descreva as linguagens reconhecidas pelos AFDs seguintes:
<ol>
    <li>
    $$
\begin{array}{lc|ccc}
    &   q   &   0   &   1   &   \lambda \\
\hline
IF  &   0   &   1   &   3           \\
    &   1   &   2                   \\
    &   2   &   0                   \\
    &   3   &       &   5   &   4   \\
    &   4   &       &   0    \\
    &   5   &       &   4 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|ccc}
    &   q   &   0   &   1   &   \lambda \\
\hline
I   &   0   &   1   &   1           \\
    &   1   &   2                   \\
    &   2   &   2   &   2, 3        \\
F   &   3   &       &       &   0 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|ccc}
    &   q   &   0   &   1   &   \lambda \\
\hline
I   &   0   &   1,2 &       &           \\
    &   1   &       &   3   &           \\
    &   2   &   4   &       &           \\
    &   3   &       &   5   &   1       \\
    &   4   &       &   5   &   2       \\
F   &   5   &       &       &   0   
\end{array}
    $$
    </li>
</ol> 
'''),
('Exercício 16',
r'''Considere a linguagem de todos os números inteiros sem sinal, escritos em base $$3$$, em que o último algarismo ocorre anteriormente no número. Por exemplo, $$1202$$ e $$00$$ pertencem a esta linguagem, mas $$1002$$ e $$1$$ não. (Os números escritos em base $$3$$ só podem ter os algarismos $$0, 1$$ e $$2$$.)
<ol>
<li>Escreva uma expressão regular que a represente.</li>
<li>Defina um autómato finito que a reconheça.</li>
<li>Apresente um autómato finito determinista que a reconheça (Sugestão: tente construí-lo diretamente, sem recorrer a qualquer algoritmo).</li>
</ol>
'''),
('Exercício 17',
r'''Considere o AFND definido pela tabela
<p>
$$
\begin{array}{lc|ccc}
    &   q   &   0   &   1   &   \lambda \\
\hline
I   &   0   &       & 0, 2  &    1   \\
    &   1   &   5   &       &    2   \\
    &   2   &   3   &       &        \\
F   &   3   &       &   1   &   4, 5 \\
F   &   4   &   3   &       &        \\
    &   5   &       &   4   &       
\end{array}
$$
<ol>
<li>Calcule o $$\lambda\text{-fecho}\left(\left\{ 0 \right\} \right)$$ e o $$\lambda\text{-fecho}\left(\left\{ 1, 2, 3 \right\} \right)$$.</li>
<li>Desenhe as árvores das computações de $$011$$ e $$101$$, indicando se são aceites ou rejeitadas.</li>
</ol>
'''),
('Exercício 18',
r'''Seja $$A = \left( \left\{ 0, 1, 2 \right\}, \left\{ a, b, c \right\}, \delta, 0, \left\{ 1, 2 \right\} \right)$$ um autómato finito não determinista cuja função de transição é definida por
<p>
$$
\begin{array}{c|cccc}
\delta & a & b & c & \lambda \\
\hline
0 & 0 &   &  1 &  2 \\
1 &   &   &  1 \\
2 &   & 1, 2 
\end{array}
$$
<p>
Construa um autómato finito determinista equivalente a $$A$$, usando o algoritmo dado nas aulas, e apresente uma expressão regular que represente a linguagem por ele reconhecida.
'''),
('Exercício 19',
r'''Seja $$A = \left( \left\{ 0, 1, 2, 3 \right\}, \left\{ m, n \right\}, \delta, 0, \left\{ 1 \right\} \right)$$ um autómato finito não determinista cuja função de transição é definida por
<p>
$$
\begin{array}{c|ccc}
    \delta & m & n & \lambda \\
    \hline
    0 &  1 &  2 &  1 \\
    1 &    &  1, 3   \\
    2 &    &    &  3 \\
    3 &  1, 3 &  2 
    \end{array}
$$
<p>
Construa um autómato finito determinista equivalente a $$A$$, usando o algoritmo dado nas aulas, e apresente uma expressão regular que represente a linguagem por ele reconhecida.
'''),
('Exercício 20',
r'''Considere a linguagem de todas as palavras sobre $$\left\{ a, b \right\}$$ em que o antepenúltimo símbolo é $$b$$.
<ol>
<li>Defina um autómato finito (não determinista) que reconheça esta linguagem.</li>
<li>Aplicando o algoritmo dado nas aulas, construa um autómato finito determinista equivalente ao autómato finito da alínea anterior.</li>
</ol>'''),
('Exercício 21',
r'''Considere a linguagem de todas as palavras sobre $$\left\{ a, b \right\}$$ em que o terceiro e o antepenúltimo símbolos são $$b$$. São exemplos de palavras que pertencem a esta linguagem as palavras $$aababaa$$, $$abbbbbbbb$$, $$abba$$ e $$bbb$$..
<ol>
<li>Defina um autómato finito (não determinista) que reconheça esta linguagem.</li>
<li>Aplicando o algoritmo dado nas aulas, construa um autómato finito determinista equivalente ao autómato finito da alínea anterior.</li>
</ol>'''),
('Exercício 22',
r'''Sejam $$A_1 = \left( Q_1, T, \delta_1, q_{01}, F_1 \right)$$ e $$A_2 = \left( Q_2, T, \delta_2, q_{02}, F_2 \right)$$ dois autómatos finitos tais que $$Q_1$$ e $$Q_2$$ são disjuntos. Defina um autómato finito $$B$$ tal que $${\cal L}\left( B \right) = {\cal L}\left( A_1 \right) \cup {\cal L}\left( A_2 \right)$$.'''),
('Exercício 23',
r'''Defina um AFD equivalente ao AFND $$N=\left( \left\{ p, q, r \right\},\left\{ 0, 1 \right\}, \delta, p, \left\{ q, r \right\} \right)$$ em que
<p>
$$
\begin{array}{c|cc}
\delta & 0 & 1 \\
\hline
p &  p, q &  p \\
q &  &  r \\
r & 
\end{array}
$$''')
]

MAFD = [
('Exercício 24',
r'''Aplicando o algoritmo dado nas aulas, construa o autómato finito determinista mínimo equivalente a $$A = \left( \left\{ A, B, C, D, E, F \right\}, \left\{ a, b \right\}, \delta, A, \left\{ B, D \right\} \right)$$, com a função de transição
<p>
$$
\begin{array}{c|cc}
\delta  & a & b \\
\hline
A   &   B   &   E   \\
B   &   E   &   C   \\
C   &   D   &   F   \\
D   &   F   &   C   \\
E   &   F   &   F   \\
F   &   E   &
\end{array}
$$
<p>
e apresente uma expressão regular que represente $${\cal L}\left( A \right)$$.
'''),
('Exercício 25',
r'''Considere o autómato finito não determinista $$A =  \left( \left\{ 1, 2, 3, 4, 5 \right\}, \left\{ x, y \right\}, \delta, 1, \left\{ 5 \right\} \right) $$, com a função de transição
<p>
$$
\begin{array}{c|cc}
\delta  & x     & y     \\
\hline
1 &  1 &  1, 2 \\
2 &  &  3 \\
3 &  3 &  3, 4 \\
4 &  &  5 \\
5 &  5 &  5 
\end{array}
$$
<ol>
<li>Construa o AFD equivalente a $$A$$.</li>
<li>Construa o AFD mínimo equivalente a $$A$$.</li>
</ol>
'''),
('Exercício 26',
r'''Considere a linguagem representada por $$(aaa)^* $$ reunida com $$(aa)^* $$.
<ol>
<li>Defina um autómato finito (não determinista) que reconheça aquela linguagem.</li>
<li>Construa o AFD equivalente ao autómato finito definido na alínea anterior.</li>
<li>Construa o AFD mínimo equivalente ao AFD obtido na alínea anterior.</li>
</ol>
'''),
('Exercício 27',
r'''Considere a linguagem das palavras sobre $$\left\{ a, b, c \right\}$$ que têm $$ababc$$ como subpalavra.<ol>
<li>Defina um autómato finito (não determinista) que reconheça aquela linguagem.</li>
<li>Construa o AFD equivalente ao autómato finito definido na alínea anterior.</li>
<li>Construa o AFD mínimo equivalente ao AFD obtido na alínea anterior.</li>
</ol>
''')
]

CAFND = [
('Exercício 28',
r'''Construa um AFND que aceita a linguagem das palavras:
<ol>
<li>Binárias com, pelo menos, três ocorrências de $$010$$.</li>
<li>Binárias com subpalavras $$010$$ e $$101$$.</li>
<li>Binárias (com subpalava $$010$$ ou $$101$$) e (com sufixo $$111$$ ou $$000$$).</li>
<li>Binárias em que o $$3n$$-ésio símbolo é $$0$$, para $$n\geq 1$$.</li>
<li>Binárias, de tamanho $$3n$$, e em que, para cada $$1\leq k \leq n$$, algum dos $$3k-2$$, $$3k-1$$ ou $$3k$$-ésimo símbolo é $$0$$.</li>
<li>$$0^n 1 0^m 1 0^q$$ em que $$q\equiv nm (\bmod 3)$$.</li>
</ol>
'''),
('Exercício 29',
r'''Encontre um AFND que aceite as palavra com prefixo $$010$$ ou sufixo $$110$$.'''),
('Exercício 30',
r'''Encontre um AFND que aceite as palavra com, pelo menos, duas ocorrências de $$01$$ e sufixo $$11$$.'''),
('Exercício 31',
r'''Encontre um AFND que aceite as palavras $$0^n 1 0^m$$, $$n \equiv m (\bmod 3)$$.
'''),
('Exercício 32',
r'''Sejam $$A_1$$ e $$A_2$$ dois AFNDs. Encontre um AFND $$A$$ tal que $${\cal L}\left( A \right) = {\cal L}\left( A_1 \right){\cal L}\left( A_2 \right)$$.
'''),
('Exercício 33',
r'''Seja $$A$$ um AFND. Encontre um AFND $$B$$ tal que $${\cal L}\left( B \right) = {\cal L}\left( A \right)^* $$.'''),
('Exercício 34',
r'''Seja $$M=\left( \left\{ p, q, r, s \right\}, \left\{ 0, 1 \right\}, \delta, p, \left\{ q, s \right\} \right)$$ um AFND em
    $$
    \begin{array}{c|cc}
    \delta & 0 & 1 \\
    \hline
    p   &  q, s &  q \\
    q   &  r   &  q, r \\
    r   &  s   &  p \\
    s   &  &  p 
    \end{array}
    $$
    Construa um AFND que aceite $$\overline{{\cal L}\left( N \right)}$$.
'''),
('Exercício 35',
r'''<ol>
    <li>Construa um AFD que aceite a linguagem binária das palavras em que o quinto símbolo a partir do fim é $$0$$.</li>
    <li>Converta (simule) cada um dos AFND seguintes por um AFD:
        <ol>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
IF  &   0   &   1   &   3   \\
    &   1   &   4   &   3   \\
    &   2   &   1   &   0   \\
    &   3   &   2   &   4   \\
    &   4   &   0   &   2 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
I   &   0   &   1   &   2   \\
    &   1   &   0   &   3   \\
F   &   2   &   3   &   0   \\
    &   3   &   2   &   1 
\end{array}
    $$
    </li>
    <li>
    $$
\begin{array}{lc|cc}
    &   q   &   0   &   1   \\
\hline
I   &   0   &   1   &   0   \\
    &   1   &   1   &   2   \\
    &   2   &   3   &   0   \\
F   &   3   &   4   &   4   \\
    &   4   &   4   &   4 
\end{array}
    $$
    </li>
</ol> 
    </li>
    </ol>
'''),
('Exercício 36',
r'''Construa um AFD que aceite a linguagem das palavras:
    <ol>
        <li>Que contêm as sub-palavras $$010$$ e $$101$$.</li>
        <li>Com sufixo $$00$$ ou $$01$$ ou $$10$$ (de duas maneiras).</li>
        <li>Ou (contêm as sub-palavras $$001$$ e $$110$$) ou (não contêm nem $$001$$ nem $$110$$).</li>
        <li>Em que a quarta letra a partir do fim, e a quarta letra a partir do princípio são ambas $$0$$ (nota: $$0110$$ e $$10101$$ estão nesta linguagem).</li>
    </ol>
''')
]

PL = [
('Exercício 37',
r'''Mostre que a linguagem $$L = \left\{1^n+1^m=1^{n+m} ~:~ n, m \geq 0}$$ não é uma linguagem regular.'''),
('Exercício 38',
r'''Mostre que a linguagem dos palíndromos (capícuas) sobre $$\left\{ a, b \right\}$$ não é uma linguagem regular.''')
]

from mquestions import *
f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', PL))