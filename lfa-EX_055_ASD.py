GLL1 = [
#
#
#
('Exercício 01', r'''Calcule os símbolos diretores e determine se as seguintes gramáticas são $${\cal LL}(1)$$:
<ol>
<li>$$G_1 = (\left\lbrace S, A, B \right\rbrace, \left\lbrace a, b, c, \# \right\rbrace, \left\lbrace S \to AB\#, A \to aAb | B, B \to aBc | \lambda \right\rbrace, S)$$</li>
<li>$$G_2 = (\left\lbrace S, A, B, C \right\rbrace, \left\lbrace a, b, c, d, \# \right\rbrace, \left\lbrace S \to ABC\#, A \to aA | \lambda, B \to bBc | \lambda, C \to cA | dB | \lambda \right\rbrace, S)$$</li>
<li>$$\checkmark$$ $$G_3 = (\left\lbrace S, A, B, C, D \right\rbrace, \left\lbrace a, b, c, d, \# \right\rbrace, \left\lbrace S \to aAd\#, A \to BCD, B \to bB | \lambda, C \to cC | \lambda, D \to bD | \lambda \right\rbrace, S)$$</li>
</ol>'''),
#
#
#
('Exercício 02', r'''$$\dagger\checkmark$$
Modifique a gramática $$G = (\left\lbrace S, A, B, C \right\rbrace, \left\lbrace a, b, c, \# \right\rbrace, P, S)$$ de modo a obter uma gramática $${\cal LL}(1)$$ equivalente. As produções de $$G$$ são:
<p>
$$\begin{align*}
S &\to A\#\\
A &\to aB | Ab | Ac\\
B &\to bBc | \lambda
\end{align*}$$
<p>
<b>Sugestão:</b> Comece por determinar $${\cal L}\left( G \right)$$.'''),
#
#
#
('Exercício 03', r'''Modifique a gramática $$G = (\left\lbrace S, A, B, C \right\rbrace, \left\lbrace a, b, c, \# \right\rbrace, P, S)$$ de modo a obter uma gramática $${\cal LL}(1)$$ equivalente. As produções de $$G$$ são:
<p>$$\begin{align*}
S &\to aA\# | abB\# | abcC\# \\
A &\to aA | \lambda \\
B &\to bB | \lambda \\
C &\to Cc | \lambda
\end{align*}$$'''),
#
#
#
('Exercício 04', r'''Defina uma gramática $${\cal LL}(1)$$ que gere a linguagem das expressões aritméticas com subtração, multiplicação e parêntesis. (Use o símbolo $$n$$ para representar os inteiros.)'''),
]

GLR0 = [
#
#
#
('Exercício 05', r'''Determine os $$\text{contexto-}{\cal LR}\left( 0 \right)$$ da gramática com produções
<ol>
<li>$$\checkmark$$~
<p>$$\begin{align*}
S &\to aA | bB \\
A &\to abA | bB \\
B &\to bBc | bc 
\end{align*}$$</li>
<li>~
<p>$$\begin{align*}
S &\to aA | aB \\
A &\to aAb | b \\
B &\to bBa | b  
\end{align*}$$</li>
</ol>'''),
#
#
#
('Exercício 06', r'''$$\checkmark$$
Considere a gramática $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b,\# \right\rbrace, \left\lbrace S \to aBA\#, A \to b | aB, B \to a | bB \right\rbrace, S)$$.
<ol>
<li>Construa o autómato dos itens válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(0)$$.</li>
<li>Construa a tabela de análise sintática para $$G$$.</li>
<li>Defina o autómato de pilha reconhecedor $${\cal LR}(0)$$ para $$G$$.</li>
</ol>'''),
#
#
#
('Exercício 07', r'''Considere a gramática $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b,\# \right\rbrace, \left\lbrace S \to aA\# | AB\#, A \to aAb | b, B \to ab | b \right\rbrace, S)$$.
<ol>
<li>Construa o autómato dos itens válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(0)$$.</li>
<li>Se concluir que a gramática não é $${\cal LR}(0)$$, enumere os conflitos encontrados.</li>
</ol>'''),
#
#
#
('Exercício 08', r'''Considere a gramática $$G = (\left\lbrace X,Y,Z \right\rbrace, \left\lbrace 0,1,\# \right\rbrace, \left\lbrace X \to ZY\# | 1YZ\#, Y \to 0Y | \lambda, Z \to Z1 | 1 \right\rbrace, X)$$.
<ol>
<li>Construa o autómato dos itens válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(0)$$.</li>
<li>Se concluir que a gramática não é $${\cal LR}(0)$$, enumere os conflitos encontrados.</li>
</ol>'''),
]

GLR1 = [
#
#
#
('Exercício 09', r'''_$$\checkmark$$ Considere a gramática $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b \right\rbrace, P, S)$$, com o conjunto de produções:
<p>$$\begin{align*}
S &\to AB \\
A &\to aA | \lambda \\
B &\to Bb | \lambda
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
<li>Com base no autómato dos itens válidos de $$G$$, defina o autómato de pilha reconhecedor $${\cal LR}(1)$$.</li>
<li>Construa a computação do autómato de pilha para a palavra $$aabb$$.</li>
</ol>'''),
#
#
#
('Exercício 10', r'''_$$\checkmark$$ Considere a gramática $$G = (\left\lbrace S,A,B \right\rbrace, \left\lbrace a,b \right\rbrace, P, S)$$, com o conjunto de produções:
<p>$$\begin{align*}
S &\to ABA \\
A &\to Aa | \lambda \\
B &\to bB | b
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
<li>Com base no autómato dos itens válidos de $$G$$, defina o autómato de pilha reconhecedor $${\cal LR}(1)$$.</li>
<li>Construa a computação do autómato de pilha para a palavra $$aabb$$.</li>
</ol>'''),
#
#
#
('Exercício 11', r'''$$\checkmark$$ Considere a gramática  $$G = (\left\lbrace S \right\rbrace, \left\lbrace a \right\rbrace, P, S)$$, com o conjunto de produções:
<p>$$\begin{align*}
S &\to aSa | \lambda
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
</ol>
(Se $$G$$ não é $${\cal LR}(1)$$, não faça as alíneas que não se aplicam.)'''),
#
#
#
('Exercício 12', r'''Considere a gramática  $$G = (\left\lbrace S,E,F \right\rbrace, \left\lbrace w,x,y,z \right\rbrace, P, S)$$, com o conjunto de produções:
<p>$$\begin{align*}
S &\to Ex | yEw | Fw | yFx \\
E &\to z \\
F &\to z
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
</ol>
(Se $$G$$ não é $${\cal LR}(1)$$, não faça as alíneas que não se aplicam.)'''),
#
#
#
('Exercício 13', r'''Considere a gramática  $$G = (\left\lbrace S,C \right\rbrace, \left\lbrace c,d \right\rbrace, P, S)$$, com o conjunto de produções:
<p>$$\begin{align*}
S & \to CC \\
C & \to cC | d
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
</ol>
(Se $$G$$ não é $${\cal LR}(1)$$, não faça as alíneas que não se aplicam.)'''),
#
#
#
('Exercício 14', r'''Considere a gramática $$G = (\left\lbrace E,T \right\rbrace, \left\lbrace i,+,(,) \right\rbrace, P, E)$$, com o conjunto de produções:
<p>$$\begin{align*}
E &\to E+T | T \\
T &\to i | (E)
\end{align*}$$
<p>
<ol>
<li>Construa o autómato dos itens $${\cal LR}(1)$$ válidos de $$G$$.</li>
<li>Diga, justificando, se $$G$$ é $${\cal LR}(1)$$.</li>
<li>Construa o autómato amalgamado e diga, justificando, se $$G$$ é $${\cal LALR}(1)$$.</li>
<li>Construa a tabela de análise sintática $${\cal LR}(1)$$ para $$G$$.</li>
</ol>
(Se $$G$$ não é $${\cal LR}(1)$$, não faça as alíneas que não se aplicam.)'''),
]
###########################################

"""
#
#
#
('Exercício XX', r'''_'''),
"""

from mquestions import *
f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', GLR1))