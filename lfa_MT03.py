from mquestions import *

EX01 = [
    (
        'F3-' + qid(6, 1),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, A
                \\right\\rbrace,
                \left\lbrace
                    0, 1, 2
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é:<p>
        \(
            \\begin{align*}
                S &\\to 01 ~|~ 0A1 \\\\
                A &\\to 2 ~|~ AA ~|~ S
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 2),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, L
                \\right\\rbrace,
                \left\lbrace
                    a, b, c
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é:<p>
        \(
            \\begin{align*}
                S &\\to aLb ~|~ ab \\\\
                L &\\to LL ~|~ c ~|~ S
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 3),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, N
                \\right\\rbrace,
                \left\lbrace
                    a, b, n
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é:<p>
        \(
            \\begin{align*}
                S &\\to aSb ~|~ N \\\\
                N &\\to Nn ~|~ \lambda
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 4),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, A
                \\right\\rbrace,
                \left\lbrace
                    0, 1, 2
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é:<p>
        \(
            \\begin{align*}
                S &\\to A ~|~ 0S1 \\\\
                A &\\to \lambda ~|~ A2
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 5),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    E, S, T
                \\right\\rbrace,
                \left\lbrace
                    n, +, (, )
                \\right\\rbrace,
                P,
                E
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é<p>
        \(
            \\begin{align*}
                E &\\to S\\\\
                S &\\to T ~|~ S + T \\\\
                T &\\to n ~|~ (E)
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 6),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, A, B
                \\right\\rbrace,
                \left\lbrace
                    a, b, c, d
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é<p>
        \(
            \\begin{align*}
                S &\\to A\\\\
                A &\\to B ~|~ AaB \\\\
                B &\\to b ~|~ cSd
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 7),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, A
                \\right\\rbrace,
                \left\lbrace
                    a, b
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é<p>
        \(
            \\begin{align*}
                S &\\to aAb ~|~ a\\\\
                A &\\to SS ~|~ b
            \end{align*}
        \)
        </p>'''
    ),
    (
        'F3-' + qid(6, 8),
        '''Seguindo o processo dado nas aulas, obtenha a <em>Forma Normal de Griebach</em> da gramática<p>
        \(  
            G = \left(
                \left\lbrace
                    S, L
                \\right\\rbrace,
                \left\lbrace
                    0, 1
                \\right\\rbrace,
                P,
                S
            \\right)
        \)
        </p>em que o conjunto das produções, \(P\), é<p>
        \(
            \\begin{align*}
                S &\\to 0 ~|~ 0L1\\\\
                L &\\to  1 ~|~ SS
            \end{align*}
        \)
        </p>'''
    ),
]

EX02 = [
    (
        'F3-' + qid(7, 1),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                S &\\to A ~|~ SaA \\\\
                A &\\to b ~|~ cSd
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 2),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 3),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 4),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 5),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO 
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 6),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 7),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
    (
        'F3-' + qid(7, 8),
        '''Considere a gramática, \(G\) com as seguintes produções:<p>
        \(
            \\begin{align*}
                TODO
            \end{align*}
        \)
        <p>
        Calcule os diretores e diga, justificando, se a gramática é LL(0).'''
    ),
]
f = open('MT03-02.xml', 'wt', encoding='utf-8')
f.write(quiz('IMPORT', EX02))
