GIC = [
    #
    #
    #
    ('Exercício 01', r'''
Escolha três ou quatro linguagens de programação (como c, python, Java ou outras mais recentes como go ou swift) ou apenas formais (como o XML) e encontre (<em>online<em>) gramáticas que as definam.'''),
    #
    #
    #
    ('Exercício 02', r'''Defina uma gramática independente do contexto que gere a linguagem:
<ol>
    <li>$$\left\lbrace wcw^R ~:~ w \in \{ a, b\}^* \right\rbrace$$.</li>
    <li>$$\left\lbrace wc^n ~:~ w \in \{ a, b\}^*, n = \left| w \right| \right\rbrace$$.</li>
    <li>$$\left\lbrace a^i b^j c^k ~:~ k \geq 0, i + j = k \right\rbrace$$.</li>
    <li>$$\left\lbrace a^n b^m  ~:~ n, m \geq 0, m \not= n \right\rbrace$$.</li>
    <li>Dos números naturais sem zeros não significativos.</li>
</ol>'''),
    #
    #
    #
    ('Exercício 03', r'''Defina uma gramática independente do contexto que gere os reais (incluindo os negativos) em que a parte inteira é não vazia e não tem zeros não significativos e a parte decimal é não vazia e só termina em zero se for constituída por um único 0 e as partes inteira e decimal são separadas por uma vírgula."'),
    ('Exercício 04', r"'Seja $$L$$ a linguagem de todas sequências de parêntesis, curvos - ‘$$($$’ e ‘$$)$$’ - e retos - ‘$$\lbrack$$’ e ‘$$\rbrack$$’ -, bem emparelhados. Pertencem a esta linguagem palavras como $$\lambda$$, "$$()$$", "$$\lbrack\rbrack$$", "$$()\lbrack()\rbrack$$", "$$(\lbrack()\rbrack)$$" e "$$(\lbrack\rbrack\lbrack(\lbrack\rbrack)\rbrack)\lbrack\rbrack$$". Não pertencem a $$L$$ palavras como "$$\rbrack$$", "$$($$", "$$(\rbrack$$", "$$(\lbrack)\rbrack$$", "$$)($$" e "$$\lbrack()\rbrack\rbrack$$".
    <ol>
        <li> Mostre que $$L$$ não é regular.</li>
        <li> Defina uma gramática independente do contexto que gere $$L$$.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 04', r'''Seja $$L$$ a linguagem de todas sequências de parêntesis, curvos - ‘$$($$’ e ‘$$)$$’ - e retos - ‘$$\lbrack$$’ e ‘$$\rbrack$$’ -, bem emparelhados. Pertencem a esta linguagem palavras como $$\lambda$$, "$$()$$", "$$\lbrack\rbrack$$", "$$()\lbrack()\rbrack$$", "$$(\lbrack()\rbrack)$$" e "$$(\lbrack\rbrack\lbrack(\lbrack\rbrack)\rbrack)\lbrack\rbrack$$". Não pertencem a $$L$$ palavras como "$$\rbrack$$", "$$($$", "$$(\rbrack$$", "$$(\lbrack)\rbrack$$", "$$)($$" e "$$\lbrack()\rbrack\rbrack$$".
    <ol>
        <li> Mostre que $$L$$ não é regular.</li>
        <li> Defina uma gramática independente do contexto que gere $$L$$.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 05', r'''Considere um conjunto $$V$$ de variáveis e um conjunto $$F$$ de símbolos de função, cada um com uma aridade maior que ou igual a zero. Um termo é definido como:
    <ul>
        <li> uma variável é um termo.</li>
        <li> um símbolo de função de aridade 0 é um termo.</li>
        <li> se $$t_1, t_2, \ldots, t_k, k > 0$$ são termos, então, para todos os símbolos de função $$f$$ de aridade $$k$$, $$f(t_1, t_2, \ldots, t_k)$$ é um termo.</li>
        <li> nada mais é um termo.</li>
    </ul>
    Defina uma gramática independente do contexto que gere os termos descritos: use os símbolos $$v$$ e $$f$$ como representantes das variáveis e dos símbolos de função, respetivamente. Exemplos de termos são $$v$$, $$f$$, $$f(v)$$ e $$f(v,f(f(f),f))$$.'''),
    #
    #
    #
    ('Exercício 06', r'''Considere a gramática $$G = \left( \left\lbrace A \right\rbrace, \left\lbrace a,b \right\rbrace, \left\lbrace A \to AA ~|~ aAb ~|~ \lambda\right\rbrace, A \right)$$.
    <ol>
        <li> Construa uma derivação esquerda para a palavra $$aababb$$ e a respetiva árvore de derivação.</li>
        <li> Construa uma derivação direita para a palavra $$ababab$$ e a respetiva árvore de derivação.</li>
        <li> Determine se $$G$$ é ambígua. Em caso afirmativo, apresente uma gramática não ambígua equivalente.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 07', r'''Considere a gramática independente do contexto
    <p>
    $$G = \left( \left\lbrace S \right\rbrace, \left\lbrace a \right\rbrace, \left\lbrace S \to aa ~|~ SS \right\rbrace, S \right).$$
    <p>
    <ol>
        <li> Mostre que esta gramática é ambígua.</li>
        <li> Apresente uma gramática equivalente não ambígua.</li>
        <li> Apresente uma gramática regular equivalente.</li>
        <li> Apresente uma expressão regular que represente a linguagem gerada pela gramática.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 08', r'''Considere a gramática independente do contexto
    <p>
    $$G = \left(  \left\lbrace S,A \right\rbrace, \left\lbrace a \right\rbrace, \left\lbrace S \to \lambda~|~ AS, A \to \lambda~| a \right\rbrace, S \right).$$
    <p>
    <ol>
        <li> Mostre que esta gramática é ambígua.</li>
        <li> Apresente uma gramática equivalente não ambígua.</li>
        <li> Apresente uma gramática regular equivalente.</li>
        <li> Apresente uma expressão regular que represente a linguagem gerada pela gramática.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 09', r'''Defina uma gramática livre do contexto que gere a linguagem:
    <ol>
    <li> $$\left\lbrace 0^{2k}1^{k-1} ~:~ k\geq 1 \right\rbrace$$. Usando essa gramática, derive a palavra $$00001$$.</li>
    <li> $$\left\lbrace 1^{2k+1}0^{k} ~:~ k\geq 0\right\rbrace$$. Usando essa gramática, derive a palavra $$1110$$.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 10', r'''Defina uma gramática livre do contexto que gere todas as expressões regulares sobre o alfabeto $$\left\lbrace a,b \right\rbrace$$.'''),
    #
    #
    #
    ('Exercício 11', r'''Considere a gramática livre do contexto $$S\to aSaa|B; B\to bbBcc|C; C\to bc$$;
    <ol>
        <li> Derive $$a^3b^3c^3a^6$$.</li>
        <li> Mostre que esta gramática é não ambígua.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 12', r'''Considere a gramática livre do contexto $$S\to AaSbB|\lambda; A\to aA|a; B\to bB|\lambda$$;
    <ol>
        <li> Mostre que esta gramática é ambígua.</li>
        <li> Encontre uma gramática equivalente, não ambígua.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 13', r'''Defina um autómato finito que reconheça a linguagem gerada pela gramática $$S \to 01S | 0S | 11 | 1$$.''')
]

AP = [
    #
    #
    #
    ('Exercício 14', r'''Defina um autómato de pilha que reconheça a linguagem $$\left\lbrace w ~:~ w \in \left\lbrace a,b \right\rbrace^*  ,  w = w^R\right\rbrace$$. Será possível definir um autómato de pilha determinista que reconheça esta linguagem? Justifique a sua resposta.'''),
    #
    #
    #
    ('Exercício 15', r'''Considere a linguagem de todas as palavras sobre $$\left\lbrace a,b \right\rbrace$$ em que o número de $$a$$'s é igual ao número de $$b$$'s.
    <ol>
        <li>Defina um autómato de pilha não determinista que a reconheça.</li>
        <li>Defina um autómato de pilha determinista que a reconheça.</li>
    </ol>'''),
    #
    #
    #
    ('Exercício 16', r'''Defina um autómato de pilha que reconheça $$L = \left\lbrace 1^n + 1^m = 1^{m+n} ~:~ n,m \geq 0 \right\rbrace$$ e indique se esse autómato é, ou não, determinista.'''),
    #
    #
    #
    ('Exercício 17', r'''Defina um autómato de pilha que reconheça $$L = \left\lbrace a^{2n}b^{3n} ~:~ n \geq 0 \right\rbrace$$.'''),
    #
    #
    #
    ('Exercício 18', r'''Construa um autómato de pilha que reconheça a linguagem das expressões aritméticas com o operador $$+$$ e com parêntesis. Use o símbolo $$n$$ para representar os operandos atómicos, <em>i.e.</em>, as expressões terão o seguinte aspeto: $$n$$, $$n+n$$, $$(n+(n+(n)))+n$$, <em>etc</em>.''')
]
#
#
#
# ('Exercício XX', r'''_'''),
from mquestions import *
f = open('test.xml', 'wt', encoding = 'utf-8')
f.write(quiz('IMPORT', AP))